# Cookbook

Microservice that deals with recipes

## Development

### Private Go modules

To set up usage of private Go modules run:
```
git config --global url."git@gitlab.com:".insteadOf "https://gitlab.com/"
go env -w GONOSUMDB="gitlab.com/nutrimate/microservice-kit"
```

### Configuration

To configure local development environment create `.env` file with following contents:
```
AUTH_JWKS_URI=https://dietetykwchmurze-stage.eu.auth0.com/.well-known/jwks.json
DB_CONNECTION_STRING="postgres://cookbook_user:password@localhost:6001/cookbook?sslmode=disable"
PORT=3002
DEBUG=true
ALLOWED_ORIGINS=http://localhost:3001
```

### Migrating the database

Migrate the database by running 
```
migrate -path db/migrations -database "postgres://cookbook_user:password@cookbook-db:6001/cookbook?sslmode=disable" up
```
