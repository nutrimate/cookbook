BEGIN;

CREATE TABLE ingredients (
    id UUID PRIMARY KEY,
    "name" VARCHAR(255) NOT NULL UNIQUE,
    unit_id UUID NOT NULL,
    calories NUMERIC(8,4) CHECK(calories >= 0),
    proteins NUMERIC(8,4) CHECK(proteins >= 0),
    fats NUMERIC(8,4) CHECK(fats >= 0),
    carbohydrates NUMERIC(8, 4) CHECK(carbohydrates >= 0),
    FOREIGN KEY (unit_id) REFERENCES units (id)
);

COMMIT;