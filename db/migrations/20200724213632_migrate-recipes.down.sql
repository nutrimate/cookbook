BEGIN;

DELETE FROM recipe_ingredients;
DELETE FROM recipe_instructions;
DELETE FROM recipe_meal_types;
DELETE FROM recipes;
DELETE FROM ingredients;
DELETE FROM units;

DELETE FROM meal_types WHERE id = 'ad13po27-c291-11ea-b3de-0242ac130004';

COMMIT;