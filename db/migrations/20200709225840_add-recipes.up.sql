BEGIN;

CREATE TABLE meal_types (
    id UUID PRIMARY KEY,
    name VARCHAR(255) NOT NULL UNIQUE
);

INSERT INTO meal_types (id, name)
VALUES ('a79eee44-c291-11ea-b3de-0242ac130004', 'Śniadanie'),
       ('ab261bc8-c291-11ea-b3de-0242ac130004', 'Obiad'),
       ('ae6014ba-c291-11ea-b3de-0242ac130004', 'Kolacja');

CREATE TABLE recipes (
    id UUID NOT NULL PRIMARY KEY,
    name VARCHAR(50) NOT NULL UNIQUE,
    image_url VARCHAR(4000) NOT NULL UNIQUE,
    time_to_prepare SMALLINT NOT NULL,
    enabled BOOL NOT NULL
);

CREATE TABLE recipe_instructions (
    recipe_id UUID,
    step SMALLINT CHECK (step > 0 AND step <= 255),
    description VARCHAR(255) NOT NULL,
    PRIMARY KEY(recipe_id, step),
    FOREIGN KEY (recipe_id) REFERENCES recipes (id)
);

CREATE TABLE recipe_ingredients (
    recipe_id UUID,
    ingredient_id UUID,
    amount NUMERIC (8, 4) CHECK (amount > 0),
    PRIMARY KEY (recipe_id, ingredient_id),
    FOREIGN KEY (recipe_id) REFERENCES recipes (id),
    FOREIGN KEY (ingredient_id) REFERENCES ingredients (id)
);

CREATE TABLE recipe_meal_types (
    recipe_id UUID,
    meal_type_id UUID,
    PRIMARY KEY(recipe_id, meal_type_id),
    FOREIGN KEY (recipe_id) REFERENCES recipes (id),
    FOREIGN KEY (meal_type_id) REFERENCES meal_types (id)
);

COMMIT;