BEGIN;

DROP TABLE recipe_instructions;
DROP TABLE recipe_ingredients;
DROP TABLE recipe_meal_types;
DROP TABLE recipes;
DROP TABLE meal_types;

COMMIT;