package shared

import (
	"strings"
)

// NonEmptyString validates that the string is not empty and is below length limit
func NonEmptyString(name string, maxLength int, errEmpty error, errTooLong error) (string, error) {
	trimmedName := strings.TrimSpace(name)

	switch length := len(trimmedName); {
	case length <= 0:
		return "", errEmpty
	case length > maxLength:
		return "", errTooLong
	default:
		return trimmedName, nil
	}
}
