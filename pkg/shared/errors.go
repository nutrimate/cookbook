package shared

import "errors"

// ErrValidation represents a shared validation error
var ErrValidation = errors.New("validation error")
