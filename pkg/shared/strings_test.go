package shared_test

import (
	"fmt"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/nutrimate/cookbook/pkg/shared"
)

var (
	errStringEmpty   = fmt.Errorf("%w: provided string is empty", shared.ErrValidation)
	errStringTooLong = fmt.Errorf("%w: provided string is too long", shared.ErrValidation)
)

func TestNonEmptyString_ShouldReturnTrimmedString(t *testing.T) {
	result, err := shared.NonEmptyString(" test ", 5, errStringEmpty, errStringTooLong)
	assert.NoError(t, err)
	assert.Equal(t, result, "test")
}

func TestNonEmptyString_ShouldReturnError_WhenStringIsInvalid(t *testing.T) {
	cases := []struct {
		in  string
		err error
	}{
		{
			"   ",
			errStringEmpty,
		},
		{
			strings.Repeat("x", 6),
			errStringTooLong,
		},
	}
	for _, c := range cases {
		_, err := shared.NonEmptyString(c.in, 5, errStringEmpty, errStringTooLong)

		assert.Equal(t, c.err, err)
	}
}
