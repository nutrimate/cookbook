package ingredient_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/nutrimate/cookbook/pkg/ingredient"
)

func TestNewUnitAmount_ReturnsAMount(t *testing.T) {
	value, err := ingredient.NewUnitAmount(50)

	assert.NoError(t, err)
	assert.Equal(t, 50.0, float64(value))
}

func TestNewUnitAmount_ReturnsError_WhenAmountIsSmallerThanOrEqualToZero(t *testing.T) {
	invalidAmounts := []float64{0, -1}
	for _, amount := range invalidAmounts {
		_, err := ingredient.NewUnitAmount(amount)

		assert.Equal(t, ingredient.ErrInvalidUnitAmount, err)
	}
}
