package ingredient_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/nutrimate/cookbook/pkg/ingredient"
)

func TestNewNutritionalValue_ReturnsValue(t *testing.T) {
	value, err := ingredient.NewNutritionalValue(50)

	assert.NoError(t, err)
	assert.Equal(t, 50.0, float64(value))
}

func TestNewNutritionalValue_ReturnsError_WhenValueIsBelowZero(t *testing.T) {
	_, err := ingredient.NewNutritionalValue(-1)

	assert.Equal(t, ingredient.ErrInvalidNutritionalValue, err)
}
