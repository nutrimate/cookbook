package ingredient_test

import (
	"strings"
	"testing"

	"gitlab.com/nutrimate/cookbook/pkg/ingredient"

	"github.com/stretchr/testify/assert"
)

func TestNewName_ShouldCreateName(t *testing.T) {
	name, err := ingredient.NewName("Rice")
	assert.NoError(t, err)
	assert.Equal(t, string(name), "Rice")
}

func TestNewName_ShouldReturnError_WhenNameIsInvalid(t *testing.T) {
	cases := []struct {
		in  string
		err error
	}{
		{
			"   ",
			ingredient.ErrNameTooShort,
		},
		{
			strings.Repeat("x", 256),
			ingredient.ErrNameTooLong,
		},
	}
	for _, c := range cases {
		_, err := ingredient.NewName(c.in)

		assert.Equal(t, c.err, err)
	}
}
