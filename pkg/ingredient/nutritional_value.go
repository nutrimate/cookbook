package ingredient

import (
	"fmt"

	"gitlab.com/nutrimate/cookbook/pkg/shared"
)

// NutritionalValue represents on of nutritional values of an ingredient, e.g. calories, or fats.
type NutritionalValue float64

var (
	// ErrInvalidNutritionalValue is returned when provided value is invalid
	ErrInvalidNutritionalValue = fmt.Errorf("%w: nutritional value must be greater than or equal to 0", shared.ErrValidation)
)

// NewNutritionalValue creates ingredient nutritional value enforcing invariants
func NewNutritionalValue(value float64) (NutritionalValue, error) {
	if value < 0 {
		return 0, ErrInvalidNutritionalValue
	}

	return NutritionalValue(value), nil
}
