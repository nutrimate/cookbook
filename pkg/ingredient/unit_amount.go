package ingredient

import (
	"fmt"

	"gitlab.com/nutrimate/cookbook/pkg/shared"
)

// UnitAmount represents the amount of unit used when declaring nutrients of ingredient
type UnitAmount float64

var (
	// ErrInvalidUnitAmount is returned when provided value is invalid
	ErrInvalidUnitAmount = fmt.Errorf("%w: unit amount must be greater than 0", shared.ErrValidation)
)

// NewUnitAmount creates unit amount validating it's value
func NewUnitAmount(value float64) (UnitAmount, error) {
	if value <= 0 {
		return 0, ErrInvalidUnitAmount
	}

	return UnitAmount(value), nil
}
