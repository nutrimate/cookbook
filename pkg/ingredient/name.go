package ingredient

import (
	"fmt"

	"gitlab.com/nutrimate/cookbook/pkg/shared"
)

// Errors used when validating Name
var (
	ErrNameTooShort = fmt.Errorf("%w: ingredient name must be a non-empty string", shared.ErrValidation)
	ErrNameTooLong  = fmt.Errorf("%w: ingredient name must have at most 255 characters", shared.ErrValidation)
)

// Name represents a name of an ingredient
type Name string

// NewName creates ingredient name
func NewName(name string) (Name, error) {
	result, err := shared.NonEmptyString(name, 255, ErrNameTooShort, ErrNameTooLong)
	return Name(result), err
}
