package mockingredient

import (
	"github.com/google/uuid"

	"gitlab.com/nutrimate/cookbook/pkg/ingredient"
	mockunit "gitlab.com/nutrimate/cookbook/pkg/unit/mock"
)

// NewWithDefaults creates an ingredient.Ingredient with default field values
func NewWithDefaults() ingredient.Ingredient {
	ingredientBuilder := ingredient.Builder{
		ID:            uuid.New(),
		Name:          "Orange Juice",
		Unit:          mockunit.NewWithDefaults(),
		UnitAmount:    1,
		Calories:      50,
		Fats:          4,
		Carbohydrates: 10,
		Proteins:      2,
	}
	i, err := ingredient.New(ingredientBuilder)

	if err != nil {
		panic(err)
	}

	return i
}
