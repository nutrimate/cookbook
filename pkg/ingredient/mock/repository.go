package mockingredient

import (
	"github.com/google/uuid"
	"github.com/stretchr/testify/mock"

	"gitlab.com/nutrimate/cookbook/pkg/ingredient"
)

// Repository is a mock of unit.Repository
type Repository struct {
	mock.Mock
}

// FindAll is a mocked version of unit.Repository.FindAll
func (m *Repository) FindAll() ([]ingredient.Ingredient, error) {
	args := m.Called()

	return args.Get(0).([]ingredient.Ingredient), args.Error(1)
}

// FindByIDs is a mocked version of unit.Repository.FindByIDs
func (m *Repository) FindByIDs(ids []uuid.UUID) ([]ingredient.Ingredient, error) {
	args := m.Called(ids)

	return args.Get(0).([]ingredient.Ingredient), args.Error(1)
}

// Create is a mocked version of ingredient.Repository.Create
func (m *Repository) Create(i ingredient.Ingredient) error {
	args := m.Called(i)

	return args.Error(0)
}

// Update is a mocked version of unit.Repository.Create
func (m *Repository) Update(i ingredient.Ingredient) error {
	args := m.Called(i)

	return args.Error(0)
}
