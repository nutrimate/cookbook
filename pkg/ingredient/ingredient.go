package ingredient

import (
	"github.com/google/uuid"

	"gitlab.com/nutrimate/cookbook/pkg/unit"
)

// Ingredient is a substance that is used in a Recipe
type Ingredient struct {
	ID            uuid.UUID        `json:"id"`
	Name          Name             `json:"name"`
	Unit          unit.Unit        `json:"unit"`
	UnitAmount    UnitAmount       `json:"unitAmount"`
	Calories      NutritionalValue `json:"calories"`
	Proteins      NutritionalValue `json:"proteins"`
	Fats          NutritionalValue `json:"fats"`
	Carbohydrates NutritionalValue `json:"carbohydrates"`
}

// Builder is used to create an Ingredient
type Builder struct {
	ID            uuid.UUID
	Name          string
	Unit          unit.Unit
	UnitAmount    float64
	Calories      float64
	Proteins      float64
	Fats          float64
	Carbohydrates float64
}

// New creates new ingredient
func New(builder Builder) (Ingredient, error) {
	name, err := NewName(builder.Name)
	if err != nil {
		return Ingredient{}, err
	}

	unitAmount, err := NewUnitAmount(builder.UnitAmount)
	if err != nil {
		return Ingredient{}, err
	}

	calories, err := NewNutritionalValue(builder.Calories)
	if err != nil {
		return Ingredient{}, err
	}

	proteins, err := NewNutritionalValue(builder.Proteins)
	if err != nil {
		return Ingredient{}, err
	}

	fats, err := NewNutritionalValue(builder.Fats)
	if err != nil {
		return Ingredient{}, err
	}

	carbohydrates, err := NewNutritionalValue(builder.Carbohydrates)
	if err != nil {
		return Ingredient{}, err
	}

	ingredient := Ingredient{
		ID:            builder.ID,
		Name:          name,
		Unit:          builder.Unit,
		UnitAmount:    unitAmount,
		Calories:      calories,
		Proteins:      proteins,
		Fats:          fats,
		Carbohydrates: carbohydrates,
	}

	return ingredient, nil
}
