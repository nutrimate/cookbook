package ingredient_test

import (
	"testing"

	"github.com/google/uuid"

	"gitlab.com/nutrimate/cookbook/pkg/ingredient"
	"gitlab.com/nutrimate/cookbook/pkg/unit"

	"github.com/stretchr/testify/assert"
)

func TestNew_ShouldCreateIngredient(t *testing.T) {
	// Given
	createdUnit, err := unit.New(uuid.New(), "ml", map[string]string{"1": "ml"})
	assert.NoError(t, err)
	builder := ingredient.Builder{
		ID:            uuid.New(),
		Name:          "Milk",
		Unit:          createdUnit,
		UnitAmount:    1,
		Calories:      100,
		Proteins:      5,
		Fats:          3,
		Carbohydrates: 20,
	}

	// When
	createdIngredient, err := ingredient.New(builder)

	// Then
	assert.NoError(t, err)
	assert.Equal(t, builder.ID, createdIngredient.ID)

	assert.Equal(t, builder.Name, string(createdIngredient.Name))
	assert.Equal(t, builder.Calories, float64(createdIngredient.Calories))
	assert.Equal(t, builder.Proteins, float64(createdIngredient.Proteins))
	assert.Equal(t, builder.Fats, float64(createdIngredient.Fats))
	assert.Equal(t, builder.Carbohydrates, float64(createdIngredient.Carbohydrates))
}

func TestNew_ShouldReturnError_WhenBuilderContainsInvalidValues(t *testing.T) {
	// Given
	createdUnit, err := unit.New(uuid.New(), "ml", map[string]string{"1": "ml"})
	assert.NoError(t, err)
	invalidBuilders := []ingredient.Builder{
		{
			ID:            uuid.New(),
			Name:          "",
			Unit:          createdUnit,
			UnitAmount:    1,
			Calories:      100,
			Proteins:      0,
			Fats:          0,
			Carbohydrates: 0,
		},
		{
			ID:            uuid.New(),
			Name:          "Water",
			Unit:          createdUnit,
			UnitAmount:    1,
			Calories:      -1,
			Proteins:      0,
			Fats:          0,
			Carbohydrates: 0,
		},
	}

	for _, builder := range invalidBuilders {
		// When
		_, err = ingredient.New(builder)

		// Then
		assert.Error(t, err)
	}
}
