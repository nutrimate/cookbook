package ingredient

import "github.com/google/uuid"

// Repository interface represents a place where ingredients are stored
type Repository interface {
	FindAll() ([]Ingredient, error)
	FindByIDs(ids []uuid.UUID) ([]Ingredient, error)
	Create(u Ingredient) error
	Update(u Ingredient) error
}
