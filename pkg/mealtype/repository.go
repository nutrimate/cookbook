package mealtype

import "github.com/google/uuid"

// Repository interface allows accessing persisted meal types
type Repository interface {
	FindAll() ([]MealType, error)
	FindByIDs(ids []uuid.UUID) ([]MealType, error)
}
