package mockmealtype

import (
	"github.com/google/uuid"
	"github.com/stretchr/testify/mock"

	"gitlab.com/nutrimate/cookbook/pkg/mealtype"
)

// Repository is a mock of mealtype.Repository
type Repository struct {
	mock.Mock
}

// FindAll is a mocked version of mealtype.Repository.FindAll
func (m *Repository) FindAll() ([]mealtype.MealType, error) {
	args := m.Called()

	return args.Get(0).([]mealtype.MealType), args.Error(1)
}

// FindByIDs is a mocked version of mealtype.Repository.FindByIDs
func (m *Repository) FindByIDs(ids []uuid.UUID) ([]mealtype.MealType, error) {
	args := m.Called(ids)

	return args.Get(0).([]mealtype.MealType), args.Error(1)
}
