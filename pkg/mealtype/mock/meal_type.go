package mockmealtype

import (
	"github.com/google/uuid"

	"gitlab.com/nutrimate/cookbook/pkg/mealtype"
)

// NewWithDefaults creates a mealtype.MealType with default field values
func NewWithDefaults() mealtype.MealType {
	return mealtype.MealType{
		ID:   uuid.New(),
		Name: "meal",
	}
}
