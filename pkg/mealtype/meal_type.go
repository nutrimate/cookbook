package mealtype

import "github.com/google/uuid"

// MealType represents one of daily meals, e.g. Breakfast, Lunch, Dinner.
type MealType struct {
	ID   uuid.UUID `json:"id"`
	Name string    `json:"name"`
}
