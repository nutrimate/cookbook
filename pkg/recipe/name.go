package recipe

import (
	"fmt"

	"gitlab.com/nutrimate/cookbook/pkg/shared"
)

// Errors used when validating Name
var (
	ErrNameTooShort = fmt.Errorf("%w: recipe name must be a non-empty string", shared.ErrValidation)
	ErrNameTooLong  = fmt.Errorf("%w: recipe name must have at most 80 characters", shared.ErrValidation)
)

// Name represents a name of a recipe
type Name string

// NewName creates recipe name
func NewName(name string) (Name, error) {
	result, err := shared.NonEmptyString(name, 80, ErrNameTooShort, ErrNameTooLong)
	return Name(result), err
}
