package recipe_test

import (
	"testing"

	"gitlab.com/nutrimate/cookbook/pkg/recipe"

	"github.com/stretchr/testify/assert"

	mockingredient "gitlab.com/nutrimate/cookbook/pkg/ingredient/mock"
)

func TestNewIngredientAmount_ShouldReturnAmount(t *testing.T) {
	amount, err := recipe.NewIngredientAmount(1.5)

	assert.NoError(t, err)
	assert.Equal(t, 1.5, float64(amount))
}

func TestNewIngredientAmount_ShouldReturnError_WhenAmountIsSmallerThanOrEqualToZero(t *testing.T) {
	invalidAmounts := []float64{0, -1}
	for _, invalidAmount := range invalidAmounts {
		_, err := recipe.NewIngredientAmount(invalidAmount)

		assert.Equal(t, err, recipe.ErrIngredientAmountTooSmall)
	}
}

func TestNewIngredients_ShouldCreateListOfIngredients(t *testing.T) {
	builder := []recipe.IngredientBuilder{
		{
			Ingredient: mockingredient.NewWithDefaults(),
			Amount:     10,
		},
		{
			Ingredient: mockingredient.NewWithDefaults(),
			Amount:     4,
		},
		{
			Ingredient: mockingredient.NewWithDefaults(),
			Amount:     1,
		},
	}

	recipeIngredients, err := recipe.NewIngredients(builder)

	assert.NoError(t, err)
	assert.Equal(t, len(builder), len(recipeIngredients))
	for index, recipeIngredient := range recipeIngredients {
		assert.Equal(t, builder[index].Ingredient, recipeIngredient.Ingredient)
		assert.Equal(t, builder[index].Amount, float64(recipeIngredient.Amount))
	}
}

func TestNewIngredients_ShouldReturnError_WhenIngredientsAreDuplicated(t *testing.T) {
	firstIngredient := mockingredient.NewWithDefaults()
	secondIngredient := mockingredient.NewWithDefaults()
	builder := []recipe.IngredientBuilder{
		{
			Ingredient: firstIngredient,
			Amount:     10,
		},
		{
			Ingredient: secondIngredient,
			Amount:     4,
		},
		{
			Ingredient: firstIngredient,
			Amount:     1,
		},
	}

	_, err := recipe.NewIngredients(builder)
	assert.Equal(t, recipe.ErrIngredientDuplicated, err)
}
