package mockrecipe

import (
	"github.com/google/uuid"

	mockingredient "gitlab.com/nutrimate/cookbook/pkg/ingredient/mock"
	"gitlab.com/nutrimate/cookbook/pkg/mealtype"
	mockmealtype "gitlab.com/nutrimate/cookbook/pkg/mealtype/mock"
	"gitlab.com/nutrimate/cookbook/pkg/recipe"
)

// NewWithDefaults creates a recipe.Recipe with default field values
func NewWithDefaults() recipe.Recipe {
	builder := recipe.Builder{
		ID:            uuid.New(),
		Name:          "recipe",
		TimeToPrepare: 10,
		Enabled:       true,
		MealTypes: []mealtype.MealType{
			mockmealtype.NewWithDefaults(),
			mockmealtype.NewWithDefaults(),
		},
		Instructions: []recipe.InstructionBuilder{
			{
				Step:        1,
				Description: "Cut the onions",
			},
		},
		Ingredients: []recipe.IngredientBuilder{
			{
				Ingredient: mockingredient.NewWithDefaults(),
				Amount:     1,
			},
		},
	}
	r, err := recipe.New(builder)
	if err != nil {
		panic(err)
	}

	return r
}
