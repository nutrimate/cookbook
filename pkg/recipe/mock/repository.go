package mockrecipe

import (
	"github.com/stretchr/testify/mock"

	"gitlab.com/nutrimate/cookbook/pkg/recipe"
)

// Repository is a mock of recipe.Repository
type Repository struct {
	mock.Mock
}

// FindAll is a mocked version of recipe.Repository.FindAll
func (m *Repository) FindAll() ([]recipe.Recipe, error) {
	args := m.Called()

	return args.Get(0).([]recipe.Recipe), args.Error(1)
}

// Create is a mocked version of recipe.Repository.Create
func (m *Repository) Create(r recipe.Recipe) error {
	args := m.Called(r)

	return args.Error(0)
}

// Update is a mocked version of recipe.Repository.Update
func (m *Repository) Update(r recipe.Recipe) error {
	args := m.Called(r)

	return args.Error(0)
}
