package recipe_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"github.com/google/uuid"

	"gitlab.com/nutrimate/cookbook/pkg/mealtype"
	"gitlab.com/nutrimate/cookbook/pkg/recipe"
)

func TestNewMealTypes_ShouldReturnMealTypes(t *testing.T) {
	mealTypes := []mealtype.MealType{
		{
			ID:   uuid.New(),
			Name: "Breakfast",
		},
		{
			ID:   uuid.New(),
			Name: "Breakfast",
		},
	}

	result, err := recipe.NewMealTypes(mealTypes)

	assert.NoError(t, err)
	assert.Len(t, result, 2)
	assert.Equal(t, mealTypes[0].ID, result[0].ID)
	assert.Equal(t, mealTypes[1].ID, result[1].ID)
}

func TestNewMealTypes_ShouldReturnError_WhenInputSliceIsInvalid(t *testing.T) {
	testCases := []struct {
		in  []mealtype.MealType
		err error
	}{
		{
			in:  []mealtype.MealType{},
			err: recipe.ErrMealTypesEmpty,
		},
		{
			in: []mealtype.MealType{
				{
					ID:   uuid.MustParse("00000000-0000-0000-0000-000000000001"),
					Name: "Lunch",
				},
				{
					ID:   uuid.MustParse("00000000-0000-0000-0000-000000000001"),
					Name: "Lunch",
				},
			},
			err: recipe.ErrMealTypesDuplicated,
		},
	}

	for _, testCase := range testCases {
		_, err := recipe.NewMealTypes(testCase.in)

		assert.Equal(t, testCase.err, err)
	}
}
