package recipe_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	mockingredient "gitlab.com/nutrimate/cookbook/pkg/ingredient/mock"
	"gitlab.com/nutrimate/cookbook/pkg/mealtype"

	"github.com/google/uuid"

	"gitlab.com/nutrimate/cookbook/pkg/recipe"
)

func TestNew_ShouldCreateRecipe(t *testing.T) {
	imageUrl := "https://example.com/risotto.png"
	builder := recipe.Builder{
		ID:            uuid.New(),
		Name:          "Risotto",
		ImageURL:      &imageUrl,
		TimeToPrepare: 60,
		Enabled:       true,
		Instructions: []recipe.InstructionBuilder{
			{
				Step:        1,
				Description: "Prepare rice",
			},
			{
				Step:        2,
				Description: "Mix with other ingredients",
			},
		},
		Ingredients: []recipe.IngredientBuilder{
			{
				Amount:     1,
				Ingredient: mockingredient.NewWithDefaults(),
			},
			{
				Amount:     1.5,
				Ingredient: mockingredient.NewWithDefaults(),
			},
			{
				Amount:     2,
				Ingredient: mockingredient.NewWithDefaults(),
			},
		},
		MealTypes: []mealtype.MealType{
			{
				ID:   uuid.UUID{},
				Name: "Lunch",
			},
		},
	}

	r, err := recipe.New(builder)

	assert.NoError(t, err)
	assert.Equal(t, builder.ID, r.ID)
	assert.Equal(t, builder.Name, string(r.Name))
	assert.Len(t, r.Instructions, 2)
	assert.Len(t, r.Ingredients, 3)
	assert.Len(t, r.MealTypes, 1)
}
