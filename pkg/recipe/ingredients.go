package recipe

import (
	"fmt"

	"github.com/google/uuid"

	"gitlab.com/nutrimate/cookbook/pkg/ingredient"
	"gitlab.com/nutrimate/cookbook/pkg/shared"
)

// Errors used when validating recipe ingredients
var (
	ErrIngredientAmountTooSmall = fmt.Errorf("%w: ingredient amount must be greater than 0", shared.ErrValidation)
	ErrIngredientDuplicated     = fmt.Errorf("%w: an ingredient cannot be repeated in a recipe", shared.ErrValidation)
)

// IngredientAmount represents amount of ingredient used in a recipe
type IngredientAmount float64

// NewIngredientAmount creates ingredient amount
func NewIngredientAmount(amount float64) (IngredientAmount, error) {
	if amount <= 0 {
		return 0, ErrIngredientAmountTooSmall
	}

	return IngredientAmount(amount), nil
}

// Ingredient contains ingredient with it's amount used in a recipe
type Ingredient struct {
	Amount     IngredientAmount      `json:"amount"`
	Ingredient ingredient.Ingredient `json:"ingredient"`
}

// NewIngredient creates a recipe ingredient
func NewIngredient(ingredient ingredient.Ingredient, amount float64) (Ingredient, error) {
	ingredientAmount, err := NewIngredientAmount(amount)
	if err != nil {
		return Ingredient{}, err
	}

	recipeIngredient := Ingredient{
		Amount:     ingredientAmount,
		Ingredient: ingredient,
	}

	return recipeIngredient, nil
}

// Ingredients represents a collection of unique ingredients used in a recipe
type Ingredients []Ingredient

// IngredientBuilder is used to create a recipe ingredient
type IngredientBuilder struct {
	Amount     float64               `json:"amount"`
	Ingredient ingredient.Ingredient `json:"ingredient"`
}

// NewIngredients creates a collection of recipe ingredients
func NewIngredients(ingredients []IngredientBuilder) (Ingredients, error) {
	wasIngredientUsed := map[uuid.UUID]bool{}
	recipeIngredients := Ingredients{}

	for _, b := range ingredients {
		i := b.Ingredient
		if wasIngredientUsed[i.ID] {
			return nil, ErrIngredientDuplicated
		}
		wasIngredientUsed[i.ID] = true

		recipeIngredient, err := NewIngredient(i, b.Amount)
		if err != nil {
			return nil, err
		}

		recipeIngredients = append(recipeIngredients, recipeIngredient)
	}

	return recipeIngredients, nil
}
