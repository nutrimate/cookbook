package recipe

import (
	"fmt"
	"sort"

	"gitlab.com/nutrimate/cookbook/pkg/shared"
)

// Errors used when validating InstructionDescription
var (
	ErrInstructionDescriptionTooShort = fmt.Errorf("%w: instruction description must be a non-empty string", shared.ErrValidation)
	ErrInstructionDescriptionTooLong  = fmt.Errorf("%w: instruction description must have at most 600 characters", shared.ErrValidation)
	ErrInstructionStepInvalid         = fmt.Errorf("%w: instruction step must be greater than zero", shared.ErrValidation)
)

// InstructionDescription describes one of the steps in a recipe
type InstructionDescription string

// NewInstructionDescription creates a new Instruction Description
func NewInstructionDescription(description string) (InstructionDescription, error) {
	result, err := shared.NonEmptyString(description, 600, ErrInstructionDescriptionTooShort, ErrInstructionDescriptionTooLong)

	return InstructionDescription(result), err
}

// Instruction is an ordered step of recipe with description
type Instruction struct {
	Step        uint8                  `json:"step"`
	Description InstructionDescription `json:"description"`
}

// NewInstruction creates recipe instruction
func NewInstruction(step uint8, description string) (Instruction, error) {
	instructionDescription, err := NewInstructionDescription(description)
	if err != nil {
		return Instruction{}, err
	}

	if step < 1 {
		return Instruction{}, ErrInstructionStepInvalid
	}

	i := Instruction{
		Step:        step,
		Description: instructionDescription,
	}

	return i, nil
}

// Errors used when validating a set of instructions
var (
	ErrInstructionsEmpty          = fmt.Errorf("%w: instructions are empty", shared.ErrValidation)
	ErrInstructionsStepDuplicated = fmt.Errorf("%w: instruction step can appear only once", shared.ErrValidation)
	ErrInstructionsNotContinuous  = fmt.Errorf("%w: instruction steps must be continuous", shared.ErrValidation)
)

// Instructions represents ordered collection of instructions executed when following a recipe
type Instructions []Instruction

// Len implements sort.Interface.Len
func (i Instructions) Len() int { return len(i) }

// Swap implements sort.Interface.Swap
func (i Instructions) Swap(a, b int) { i[a], i[b] = i[b], i[a] }

// Less implements sort.Interface.Less
func (i Instructions) Less(a, b int) bool { return i[a].Step < i[b].Step }

// InstructionBuilder is used to create an Instruction from basic types
type InstructionBuilder struct {
	Step        uint8  `json:"step"`
	Description string `json:"description"`
}

// NewInstructions creates collection of instructions validating it's size and uniqueness
func NewInstructions(sourceInstructions []InstructionBuilder) (Instructions, error) {
	instructions := Instructions{}

	if len(sourceInstructions) == 0 {
		return nil, ErrInstructionsEmpty
	}

	for _, b := range sourceInstructions {
		instruction, err := NewInstruction(b.Step, b.Description)
		if err != nil {
			return nil, err
		}

		instructions = append(instructions, instruction)
	}

	sort.Sort(instructions)

	for index, instruction := range instructions {
		expectedStep := uint8(index + 1)

		if index > 0 && instructions[index-1].Step == instruction.Step {
			return instructions, ErrInstructionsStepDuplicated
		}

		if expectedStep != instruction.Step {
			return nil, ErrInstructionsNotContinuous
		}
	}

	return instructions, nil
}
