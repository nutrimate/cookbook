package recipe

import (
	"fmt"

	"github.com/google/uuid"

	"gitlab.com/nutrimate/cookbook/pkg/mealtype"
	"gitlab.com/nutrimate/cookbook/pkg/shared"
)

// Errors used when validating recipe meal types
var (
	ErrMealTypesEmpty      = fmt.Errorf("%w: recipe must belong to at least one meal type", shared.ErrValidation)
	ErrMealTypesDuplicated = fmt.Errorf("%w: meal types must not be duplicated", shared.ErrValidation)
)

// MealTypes represents a collection of meal types the recipe belongs to
type MealTypes []mealtype.MealType

// NewMealTypes creates collection of meal types validating it's length and uniqueness
func NewMealTypes(types []mealtype.MealType) (MealTypes, error) {
	if len(types) == 0 {
		return nil, ErrMealTypesEmpty
	}

	wasSeen := map[uuid.UUID]bool{}
	for _, t := range types {
		if wasSeen[t.ID] {
			return nil, ErrMealTypesDuplicated
		}
		wasSeen[t.ID] = true
	}

	return types, nil
}
