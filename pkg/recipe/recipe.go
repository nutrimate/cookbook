package recipe

import (
	"github.com/google/uuid"

	"gitlab.com/nutrimate/cookbook/pkg/mealtype"
)

// Recipe represents a recipe describing how to prepare a meal
type Recipe struct {
	ID            uuid.UUID    `json:"id"`
	Name          Name         `json:"name"`
	ImageURL      *string      `json:"imageUrl"`
	TimeToPrepare uint8        `json:"timeToPrepare"`
	Enabled       bool         `json:"enabled"`
	Instructions  Instructions `json:"instructions"`
	Ingredients   Ingredients  `json:"ingredients"`
	MealTypes     MealTypes    `json:"mealTypes"`
}

// Builder is used to create a Recipe
type Builder struct {
	ID            uuid.UUID
	Name          string
	ImageURL      *string
	TimeToPrepare uint8
	Enabled       bool
	Instructions  []InstructionBuilder
	Ingredients   []IngredientBuilder
	MealTypes     []mealtype.MealType
}

// New creates a new Recipe
func New(builder Builder) (Recipe, error) {
	recipeID := builder.ID
	name, err := NewName(builder.Name)
	if err != nil {
		return Recipe{}, err
	}

	recipeInstructions, err := NewInstructions(builder.Instructions)
	if err != nil {
		return Recipe{}, err
	}

	recipeIngredients, err := NewIngredients(builder.Ingredients)
	if err != nil {
		return Recipe{}, err
	}
	mealTypes, err := NewMealTypes(builder.MealTypes)
	if err != nil {
		return Recipe{}, err
	}

	recipe := Recipe{
		ID:            recipeID,
		Name:          name,
		ImageURL:      builder.ImageURL,
		TimeToPrepare: builder.TimeToPrepare,
		Enabled:       builder.Enabled,
		Instructions:  recipeInstructions,
		Ingredients:   recipeIngredients,
		MealTypes:     mealTypes,
	}

	return recipe, nil
}
