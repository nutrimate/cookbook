package recipe

// Repository interface represents ways to access recipes
type Repository interface {
	FindAll() ([]Recipe, error)
	Create(recipe Recipe) error
	Update(recipe Recipe) error
}
