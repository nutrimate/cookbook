package recipe_test

import (
	"strings"
	"testing"

	"gitlab.com/nutrimate/cookbook/pkg/recipe"

	"github.com/stretchr/testify/assert"
)

func TestNewName_ShouldCreateName(t *testing.T) {
	name, err := recipe.NewName("Risotto nero frutti di mare")
	assert.NoError(t, err)
	assert.Equal(t, string(name), "Risotto nero frutti di mare")
}

func TestNewName_ShouldReturnError_WhenNameIsInvalid(t *testing.T) {
	cases := []struct {
		in  string
		err error
	}{
		{
			"   ",
			recipe.ErrNameTooShort,
		},
		{
			strings.Repeat("x", 81),
			recipe.ErrNameTooLong,
		},
	}
	for _, c := range cases {
		_, err := recipe.NewName(c.in)

		assert.Equal(t, c.err, err)
	}
}
