package recipe_test

import (
	"strings"
	"testing"

	"gitlab.com/nutrimate/cookbook/pkg/recipe"

	"github.com/stretchr/testify/assert"
)

func TestNewInstructionDescription_ShouldCreateDescription(t *testing.T) {
	text := "Saute meatballs in a large frying pan over medium-high heat until browned, about 10 minutes. Remove meatballs to a plate, reserving the pan drippings."
	description, err := recipe.NewInstructionDescription(text)
	assert.NoError(t, err)
	assert.Equal(t, string(description), text)
}

func TestNewInstructionDescription_ShouldReturnError_WhenDescriptionIsInvalid(t *testing.T) {
	cases := []struct {
		in  string
		err error
	}{
		{
			"   ",
			recipe.ErrInstructionDescriptionTooShort,
		},
		{
			strings.Repeat("x", 601),
			recipe.ErrInstructionDescriptionTooLong,
		},
	}
	for _, c := range cases {
		_, err := recipe.NewInstructionDescription(c.in)

		assert.Equal(t, c.err, err)
	}
}

func TestNewInstruction_ShouldReturnInstruction(t *testing.T) {
	step := uint8(1)
	description := "Cut the onions"
	i, err := recipe.NewInstruction(step, description)

	assert.NoError(t, err)
	assert.Equal(t, step, i.Step)
	assert.Equal(t, description, string(i.Description))
}

func TestNewInstruction_ShouldReturnError_WhenStepIsSmallerThanOne(t *testing.T) {
	_, err := recipe.NewInstruction(0, "Cut the onions")

	assert.Equal(t, recipe.ErrInstructionStepInvalid, err)
}

func TestNewInstructions_ShouldReturnOrderedCollectionOfInstructions(t *testing.T) {
	builder := []recipe.InstructionBuilder{
		{
			Step:        1,
			Description: "Execute step 1",
		},
		{
			Step:        2,
			Description: "Execute step 2",
		},
		{
			Step:        3,
			Description: "Execute step 3",
		},
	}

	result, err := recipe.NewInstructions(builder)

	assert.NoError(t, err)
	assert.Len(t, result, 3)
	for index, i := range result {
		expectedStep := uint8(index + 1)
		assert.Equal(t, expectedStep, i.Step)
		assert.Equal(t, builder[index].Description, string(i.Description))
	}
}

func TestNewInstructions_ShouldReturnError_WhenInputIsEmpty(t *testing.T) {
	builder := []recipe.InstructionBuilder{}
	_, err := recipe.NewInstructions(builder)

	assert.Equal(t, recipe.ErrInstructionsEmpty, err)
}

func TestNewInstructions_ShouldReturnError_WhenInputContainsGaps(t *testing.T) {
	builder := []recipe.InstructionBuilder{
		{
			Step:        1,
			Description: "Execute step 1",
		},
		{
			Step:        3,
			Description: "Execute step 3",
		},
	}

	_, err := recipe.NewInstructions(builder)

	assert.Equal(t, recipe.ErrInstructionsNotContinuous, err)
}

func TestNewInstructions_ShouldReturnError_WhenInputContainsDuplicateSteps(t *testing.T) {
	builder := []recipe.InstructionBuilder{
		{
			Step:        1,
			Description: "Execute step 1",
		},
		{
			Step:        2,
			Description: "Execute step 2 - 1",
		},
		{
			Step:        2,
			Description: "Execute step 2 - 2",
		},
		{
			Step:        3,
			Description: "Execute step 3",
		},
	}

	_, err := recipe.NewInstructions(builder)

	assert.Equal(t, recipe.ErrInstructionsStepDuplicated, err)
}
