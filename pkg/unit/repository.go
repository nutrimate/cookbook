package unit

import "github.com/google/uuid"

// Repository interface represents a place where units are stored
type Repository interface {
	FindAll() ([]Unit, error)
	FindByID(id uuid.UUID) (Unit, error)
	Create(u Unit) error
	Update(u Unit) error
}
