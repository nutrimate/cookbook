package unit_test

import (
	"strings"
	"testing"

	"gitlab.com/nutrimate/cookbook/pkg/unit"

	"github.com/stretchr/testify/assert"
)

func TestNewName_ShouldCreateName(t *testing.T) {
	name, err := unit.NewName("g")
	assert.NoError(t, err)
	assert.Equal(t, string(name), "g")
}

func TestNewName_ShouldReturnError_WhenNameIsInvalid(t *testing.T) {
	cases := []struct {
		in  string
		err error
	}{
		{
			"   ",
			unit.ErrNameTooShort,
		},
		{
			strings.Repeat("x", 51),
			unit.ErrNameTooLong,
		},
	}
	for _, c := range cases {
		_, err := unit.NewName(c.in)

		assert.Equal(t, c.err, err)
	}
}
