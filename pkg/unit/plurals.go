package unit

import (
	"fmt"
	"strings"

	"gitlab.com/nutrimate/cookbook/pkg/shared"
)

// Errors used when validating I18NPlurals
var (
	ErrPluralsEmpty      = fmt.Errorf("%w: plurals must not be empty", shared.ErrValidation)
	ErrPluralsValueEmpty = fmt.Errorf("%w: plural value must be non-empty string", shared.ErrValidation)
)

// I18NPlurals is a dictionary that contains information on how to pluralize given Unit with i18next
type I18NPlurals map[string]string

// NewI18NPlurals create I18NPlurals collection enforcing all invariants
func NewI18NPlurals(p map[string]string) (I18NPlurals, error) {
	if p == nil || len(p) <= 0 {
		return nil, ErrPluralsEmpty
	}

	for key, value := range p {
		if strings.TrimSpace(value) == "" {
			return nil, fmt.Errorf("%w: failing key %s", ErrPluralsValueEmpty, key)
		}
	}

	return p, nil
}
