package unit

import (
	"github.com/google/uuid"
)

// Unit represents an ingredient unit
type Unit struct {
	ID      uuid.UUID   `json:"id"`
	Name    Name        `json:"name"`
	Plurals I18NPlurals `json:"plurals"`
}

// New creates a new Unit
func New(id uuid.UUID, name string, plurals map[string]string) (Unit, error) {
	unitName, err := NewName(name)
	if err != nil {
		return Unit{}, err
	}

	unitPlurals, err := NewI18NPlurals(plurals)
	if err != nil {
		return Unit{}, err
	}

	unit := Unit{
		ID:      id,
		Name:    unitName,
		Plurals: unitPlurals,
	}

	return unit, nil
}
