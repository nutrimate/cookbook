package unit_test

import (
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"

	"gitlab.com/nutrimate/cookbook/pkg/unit"
)

func TestNew_ShouldReturnError_WhenPluralsIsEmpty(t *testing.T) {
	// Given
	id := uuid.New()
	name := "g"
	var plurals map[string]string

	// When
	_, err := unit.New(id, name, plurals)

	// Then
	assert.Equal(t, err, unit.ErrPluralsEmpty)
}

func TestNew_ShouldCreateUnit(t *testing.T) {
	// Given
	id := uuid.New()
	name := "g"
	plurals := map[string]string{"1": "g"}

	// When
	_, err := unit.New(id, name, plurals)

	// Then
	assert.NoError(t, err)
}

func TestNewFromSimpleTypes_ShouldCreateUnit(t *testing.T) {
	// Given
	id := uuid.New()
	// When
	_, err := unit.New(id, "g", map[string]string{"1": "g"})

	// Then
	assert.NoError(t, err)
}
