package unit

import (
	"fmt"

	"gitlab.com/nutrimate/cookbook/pkg/shared"
)

// Errors used when validating Name
var (
	ErrNameTooShort = fmt.Errorf("%w: unit name must be a non-empty string", shared.ErrValidation)
	ErrNameTooLong  = fmt.Errorf("%w: unit name must have at most 50 characters", shared.ErrValidation)
)

// Name represents a name of a unit
type Name string

// NewName creates unit name
func NewName(name string) (Name, error) {
	result, err := shared.NonEmptyString(name, 50, ErrNameTooShort, ErrNameTooLong)
	return Name(result), err
}
