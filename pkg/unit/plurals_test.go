package unit_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/nutrimate/cookbook/pkg/unit"
)

func TestNewI18NPlurals_ShouldCreatePluralsMap(t *testing.T) {
	plurals := map[string]string{
		"1": "x",
	}
	i18nplurals, err := unit.NewI18NPlurals(plurals)

	assert.NoError(t, err)
	assert.Len(t, i18nplurals, 1)
	assert.Equal(t, i18nplurals["1"], "x")
}

func TestNewI18NPlurals_ShouldReturnError_WhenSourceIsInvalid(t *testing.T) {
	cases := []struct {
		in  map[string]string
		err error
	}{
		{
			nil,
			unit.ErrPluralsEmpty,
		},
		{
			map[string]string{},
			unit.ErrPluralsEmpty,
		},
		{
			map[string]string{"1": "x", "2": ""},
			unit.ErrPluralsValueEmpty,
		},
	}
	for _, c := range cases {
		_, err := unit.NewI18NPlurals(c.in)

		assert.Error(t, err, c.err)
	}
}
