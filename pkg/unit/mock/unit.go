package mockunit

import (
	"github.com/google/uuid"

	"gitlab.com/nutrimate/cookbook/pkg/unit"
)

// builder is used to provide fields when creating test unit
type builder struct {
	id      uuid.UUID
	name    string
	plurals map[string]string
}

// NewWithDefaults creates a unit.Unit with default field values
func NewWithDefaults() unit.Unit {
	b := builder{
		id:   uuid.New(),
		name: "ml",
		plurals: map[string]string{
			"1": "ml",
			"2": "ml",
			"3": "ml",
		},
	}

	u, err := unit.New(b.id, b.name, b.plurals)

	if err != nil {
		panic(err)
	}

	return u
}
