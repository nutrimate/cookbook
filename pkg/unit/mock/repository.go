package mockunit

import (
	"github.com/google/uuid"
	"github.com/stretchr/testify/mock"

	"gitlab.com/nutrimate/cookbook/pkg/unit"
)

// Repository is a mock of unit.Repository
type Repository struct {
	mock.Mock
}

// FindAll is a mocked version of unit.Repository.FindAll
func (m *Repository) FindAll() ([]unit.Unit, error) {
	args := m.Called()

	return args.Get(0).([]unit.Unit), args.Error(1)
}

// FindByID is a mocked version of unit.Repository.FindById
func (m *Repository) FindByID(id uuid.UUID) (unit.Unit, error) {
	args := m.Called(id)

	return args.Get(0).(unit.Unit), args.Error(1)
}

// Create is a mocked version of unit.Repository.create
func (m *Repository) Create(u unit.Unit) error {
	args := m.Called(u)

	return args.Error(0)
}

// Update is a mocked version of unit.Repository.create
func (m *Repository) Update(u unit.Unit) error {
	args := m.Called(u)

	return args.Error(0)
}
