package main

import (
	"fmt"
	"log"
	"time"

	"github.com/getsentry/sentry-go"
	_ "github.com/joho/godotenv/autoload"
	_ "github.com/lib/pq"

	"gitlab.com/nutrimate/cookbook/internal/api"
)

func captureAndFatalf(message string, err error) {
	sentry.CaptureException(err)
	log.Fatalf("%s: %v", message, err)
}

func main() {
	err := sentry.Init(sentry.ClientOptions{})
	if err != nil {
		log.Fatalf("sentry.Init: %s", err)
	}
	defer sentry.Flush(2 * time.Second)

	container, err := api.NewContainer()
	if err != nil {
		captureAndFatalf("could not create container", err)
	}

	config := container.GetConfig()
	apiServer := container.GetServer()
	addr := fmt.Sprintf(":%d", config.Port)

	err = apiServer.Start(addr)
	if err != nil {
		captureAndFatalf("running server failed", err)
	}
}
