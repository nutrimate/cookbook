module gitlab.com/nutrimate/cookbook

go 1.16

require (
	github.com/etherlabsio/healthcheck v0.0.0-20191224061800-dd3d2fd8c3f6
	github.com/getsentry/sentry-go v0.9.0
	github.com/google/uuid v1.1.1
	github.com/joho/godotenv v1.3.0
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/labstack/echo/v4 v4.2.0
	github.com/lib/pq v1.9.0
	github.com/stretchr/testify v1.7.0
	github.com/upper/db/v4 v4.1.0
	gitlab.com/nutrimate/microservice-kit v1.0.0
)
