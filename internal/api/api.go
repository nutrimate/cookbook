package api

import (
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"gitlab.com/nutrimate/microservice-kit/pkg/api"
	"gitlab.com/nutrimate/microservice-kit/pkg/auth"

	"gitlab.com/nutrimate/cookbook/internal/api/controllers"
	"gitlab.com/nutrimate/cookbook/internal/config"
)

// NewServer creates new API server
func NewServer(config config.Config, container controllers.Container) *echo.Echo {
	e := api.NewWithMiddleware()
	e.Debug = config.Debug
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: config.AllowedOrigins,
	}))

	registerControllers(e, config, container)

	return e
}

func registerControllers(server *echo.Echo, config config.Config, container controllers.Container) {
	authMiddleware := auth.NewMiddleware(auth.MiddlewareConfig{
		JWKSEndpoint: config.Auth.JWKSURI,
	})
	middlewares := api.NewControllerMiddlewares(authMiddleware)

	container.GetHealthCheckController().Register(server, middlewares)
	container.GetIngredientsController().Register(server, middlewares)
	container.GetMealTypesController().Register(server, middlewares)
	container.GetRecipesController().Register(server, middlewares)
	container.GetUnitsController().Register(server, middlewares)
}
