package requests

import (
	"net/http"

	"gitlab.com/nutrimate/cookbook/pkg/mealtype"

	"github.com/google/uuid"
	"github.com/labstack/echo/v4"

	"gitlab.com/nutrimate/cookbook/pkg/ingredient"
	"gitlab.com/nutrimate/cookbook/pkg/recipe"
)

type payloadIngredients struct {
	IngredientID *uuid.UUID `json:"ingredientId" validate:"required"`
	Amount       *float64   `json:"amount" validate:"required,gt=0"`
}

type baseRecipePayload struct {
	Name          *string              `json:"name" validate:"required"`
	ImageURL      *string              `json:"imageUrl"`
	TimeToPrepare *uint8               `json:"timeToPrepare" validate:"required"`
	Enabled       *bool                `json:"enabled" validate:"required"`
	Instructions  []string             `json:"instructions" validate:"required,min=0,dive,required"`
	Ingredients   []payloadIngredients `json:"ingredients" validate:"required,min=0"`
	MealTypes     []uuid.UUID          `json:"mealTypes" validate:"required,min=0"`
}

// CreateRecipePayload represents payload sent in request body when creating a recipe
type CreateRecipePayload struct {
	baseRecipePayload
	ID *uuid.UUID `json:"id" validate:"required"`
}

// MapCreateRecipePayloadToRecipeBuilder transforms CreateRecipePayload to recipe.Builder
func MapCreateRecipePayloadToRecipeBuilder(
	ingredientRepository ingredient.Repository,
	mealTypeRepository mealtype.Repository,
	payload CreateRecipePayload,
) (recipe.Builder, error) {
	builder, err := mapBaseRecipePayloadToBuilder(ingredientRepository, mealTypeRepository, payload.baseRecipePayload)
	if err != nil {
		return recipe.Builder{}, err
	}

	builder.ID = *payload.ID
	return builder, nil
}

// UpdateRecipePayload represents payload sent in request body when updating a recipe
type UpdateRecipePayload = baseRecipePayload

// MapUpdateRecipePayloadToRecipeBuilder transforms UpdateRecipePayload to recipe.Builder
func MapUpdateRecipePayloadToRecipeBuilder(
	ingredientRepository ingredient.Repository,
	mealTypeRepository mealtype.Repository,
	payload UpdateRecipePayload,
	recipeID uuid.UUID,
) (recipe.Builder, error) {
	builder, err := mapBaseRecipePayloadToBuilder(ingredientRepository, mealTypeRepository, payload)
	if err != nil {
		return recipe.Builder{}, err
	}

	builder.ID = recipeID
	return builder, nil
}

func mapBaseRecipePayloadToBuilder(
	ingredientRepository ingredient.Repository,
	mealTypeRepository mealtype.Repository,
	payload baseRecipePayload,
) (recipe.Builder, error) {
	recipeInstructions := []recipe.InstructionBuilder{}
	for index, description := range payload.Instructions {
		newInstruction := recipe.InstructionBuilder{
			Step:        uint8(index + 1),
			Description: description,
		}
		recipeInstructions = append(recipeInstructions, newInstruction)
	}

	recipeIngredients, err := mapPayloadIngredientsToBuilder(ingredientRepository, payload.Ingredients)
	if err != nil {
		return recipe.Builder{}, err
	}

	mealTypes, err := mealTypeRepository.FindByIDs(payload.MealTypes)
	if err != nil {
		return recipe.Builder{}, err
	}

	builder := recipe.Builder{
		Name:          *payload.Name,
		ImageURL:      payload.ImageURL,
		TimeToPrepare: *payload.TimeToPrepare,
		Enabled:       *payload.Enabled,
		Instructions:  recipeInstructions,
		Ingredients:   recipeIngredients,
		MealTypes:     mealTypes,
	}

	return builder, nil
}

func mapPayloadIngredientsToBuilder(repository ingredient.Repository, ingredients []payloadIngredients) ([]recipe.IngredientBuilder, error) {
	ingredientIDWithAmount := map[uuid.UUID]float64{}
	ingredientIDs := []uuid.UUID{}
	for _, i := range ingredients {
		ingredientID := *i.IngredientID
		if _, ok := ingredientIDWithAmount[ingredientID]; ok {
			return nil, echo.NewHTTPError(http.StatusBadRequest, "duplicate ingredient")
		}
		ingredientIDWithAmount[ingredientID] = *i.Amount
		ingredientIDs = append(ingredientIDs, ingredientID)
	}

	recipeIngredients := []recipe.IngredientBuilder{}
	loadedIngredients, err := repository.FindByIDs(ingredientIDs)
	if err != nil {
		return nil, err
	}

	for _, i := range loadedIngredients {
		builder := recipe.IngredientBuilder{
			Ingredient: i,
			Amount:     ingredientIDWithAmount[i.ID],
		}
		recipeIngredients = append(recipeIngredients, builder)
	}

	return recipeIngredients, nil
}
