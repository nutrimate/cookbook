package requests

import "github.com/google/uuid"

type baseUnitPayload struct {
	Name    *string           `json:"name" validate:"required"`
	Plurals map[string]string `json:"plurals" validate:"required,min=1,dive,required"`
}

// CreateUnitPayload represents payload sent in request body when creating a unit
type CreateUnitPayload struct {
	baseUnitPayload
	ID *uuid.UUID `json:"id" validate:"required"`
}

// UpdateUnitPayload represents payload sent in request body when updating a unit
type UpdateUnitPayload baseUnitPayload
