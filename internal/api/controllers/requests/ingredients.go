package requests

import (
	"fmt"
	"net/http"

	"github.com/google/uuid"
	"github.com/labstack/echo/v4"

	"gitlab.com/nutrimate/cookbook/pkg/ingredient"
	"gitlab.com/nutrimate/cookbook/pkg/unit"
)

type baseIngredientPayload struct {
	Name          *string    `json:"name" validate:"required"`
	UnitID        *uuid.UUID `json:"unitId" validate:"required"`
	UnitAmount    *float64   `json:"unitAmount" validate:"required,gt=0"`
	Calories      *float64   `json:"calories" validate:"required,min=0"`
	Proteins      *float64   `json:"proteins" validate:"required,min=0"`
	Fats          *float64   `json:"fats" validate:"required,min=0"`
	Carbohydrates *float64   `json:"carbohydrates" validate:"required,min=0"`
}

// CreateIngredientPayload represents payload sent in request body when creating an ingredient
type CreateIngredientPayload struct {
	baseIngredientPayload
	ID *uuid.UUID `json:"id" validate:"required"`
}

// MapCreateIngredientPayloadToIngredientBuilder transforms CreateIngredientPayload to ingredient.Builder
func MapCreateIngredientPayloadToIngredientBuilder(
	unitRepository unit.Repository,
	payload CreateIngredientPayload,
) (ingredient.Builder, error) {
	builder, err := mapBaseIngredientPayloadToBuilder(unitRepository, payload.baseIngredientPayload)
	if err != nil {
		return ingredient.Builder{}, err
	}

	builder.ID = *payload.ID
	return builder, nil
}

// UpdateIngredientPayload represents payload sent in request body when updating an ingredient
type UpdateIngredientPayload = baseIngredientPayload

// MapUpdateIngredientPayloadToIngredientBuilder transforms UpdateIngredientPayload to ingredient.Builder
func MapUpdateIngredientPayloadToIngredientBuilder(
	unitRepository unit.Repository,
	payload UpdateIngredientPayload,
	ingredientID uuid.UUID,
) (ingredient.Builder, error) {
	builder, err := mapBaseIngredientPayloadToBuilder(unitRepository, payload)
	if err != nil {
		return ingredient.Builder{}, err
	}

	builder.ID = ingredientID
	return builder, nil
}

func mapBaseIngredientPayloadToBuilder(unitRepository unit.Repository, payload baseIngredientPayload) (ingredient.Builder, error) {
	ingredientUnit, err := unitRepository.FindByID(*payload.UnitID)
	if err != nil {
		return ingredient.Builder{}, echo.NewHTTPError(http.StatusBadRequest, fmt.Sprintf("Could not fint unit with id = %s", payload.UnitID))
	}

	builder := ingredient.Builder{
		Name:          *payload.Name,
		Unit:          ingredientUnit,
		UnitAmount:    *payload.UnitAmount,
		Calories:      *payload.Calories,
		Proteins:      *payload.Proteins,
		Fats:          *payload.Fats,
		Carbohydrates: *payload.Carbohydrates,
	}

	return builder, nil
}
