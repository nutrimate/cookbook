package controllers_test

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/google/uuid"
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/mock"
	"gitlab.com/nutrimate/microservice-kit/pkg/apitest"
	"gitlab.com/nutrimate/microservice-kit/pkg/auth"

	"gitlab.com/nutrimate/cookbook/internal/api/controllers"
	"gitlab.com/nutrimate/cookbook/pkg/ingredient"
	"gitlab.com/nutrimate/cookbook/pkg/mealtype"
	mockmealtype "gitlab.com/nutrimate/cookbook/pkg/mealtype/mock"

	mockingredient "gitlab.com/nutrimate/cookbook/pkg/ingredient/mock"

	"github.com/stretchr/testify/assert"

	"gitlab.com/nutrimate/cookbook/pkg/recipe"
	mockrecipe "gitlab.com/nutrimate/cookbook/pkg/recipe/mock"
)

func TestNewRecipesController(t *testing.T) {
	type testSetup struct {
		server               *echo.Echo
		recipeRepository     *mockrecipe.Repository
		ingredientRepository *mockingredient.Repository
		mealTypeRepository   *mockmealtype.Repository
	}

	setupTest := func() testSetup {
		recipeRepository := &mockrecipe.Repository{}
		ingredientRepository := &mockingredient.Repository{}
		mealTypeRepository := &mockmealtype.Repository{}

		controller := controllers.NewRecipesController(recipeRepository, ingredientRepository, mealTypeRepository)

		server := apitest.SetupTestAPIServer(controller)

		return testSetup{
			server:               server,
			recipeRepository:     recipeRepository,
			ingredientRepository: ingredientRepository,
			mealTypeRepository:   mealTypeRepository,
		}
	}

	accessTest := apitest.NewAccessTest(func() *echo.Echo {
		return setupTest().server
	})
	user := auth.RequestUser{
		ID: "1",
		Permissions: []string{
			controllers.WriteCookbookScope,
		},
		Scope: "",
	}

	t.Run("GET /recipes", func(t *testing.T) {
		accessTest.ShouldUseAuthMiddleware(t, "GET", "/recipes")
		accessTest.ShouldCheckPermissions(t, "GET", "/recipes")

		t.Run("should respond with all recipes", func(t *testing.T) {
			// Given
			setup := setupTest()
			recipeRepository := setup.recipeRepository
			server := setup.server

			recipes := []recipe.Recipe{
				mockrecipe.NewWithDefaults(),
				mockrecipe.NewWithDefaults(),
				mockrecipe.NewWithDefaults(),
			}
			recipeRepository.On("FindAll").Return(recipes, nil)

			req := apitest.NewAuthRequestWithUser(user, "GET", "/recipes", nil)
			rec := httptest.NewRecorder()

			// When
			server.ServeHTTP(rec, req)

			// Then
			var bodyItems []recipe.Recipe

			assert.Equal(t, http.StatusOK, rec.Code)
			if err := json.Unmarshal(rec.Body.Bytes(), &bodyItems); !assert.NoError(t, err) {
				assert.Equal(t, recipes, bodyItems)
			}
		})
	})

	t.Run("POST /recipes", func(t *testing.T) {
		accessTest.ShouldUseAuthMiddleware(t, "POST", "/recipes")
		accessTest.ShouldCheckPermissions(t, "POST", "/recipes")

		t.Run("should allow creating new recipe", func(t *testing.T) {
			// Given
			setup := setupTest()
			recipeRepository := setup.recipeRepository
			ingredientRepository := setup.ingredientRepository
			mealTypeRepository := setup.mealTypeRepository
			server := setup.server

			mockIngredient := mockingredient.NewWithDefaults()
			mockIngredient.ID = uuid.MustParse("10000000-0000-0000-0000-000000000000")
			ingredientRepository.On("FindByIDs", mock.Anything).
				Return([]ingredient.Ingredient{mockIngredient}, nil)

			mockMealType := mockmealtype.NewWithDefaults()
			mockMealType.ID = uuid.MustParse("20000000-0000-0000-0000-000000000000")
			mealTypeRepository.On("FindByIDs", mock.Anything).
				Return([]mealtype.MealType{mockMealType}, nil)

			recipeRepository.On("Create", mock.Anything).Return(nil)

			payload := `
			{
				"id":"00000000-0000-0000-0000-000000000000",
				"name": "x",
				"imageUrl": "y",
				"timeToPrepare": 30,
				"enabled": true,
				"ingredients": [
					{
						"ingredientId": "10000000-0000-0000-0000-000000000000",
						"amount": 1
					}
				],
				"instructions": ["test"],
				"mealTypes": ["20000000-0000-0000-0000-000000000000"]
			}`
			req := apitest.NewAuthRequestWithUser(user, "POST", "/recipes", []byte(payload))
			rec := httptest.NewRecorder()

			// When
			server.ServeHTTP(rec, req)

			// Then
			assert.Equal(t, http.StatusCreated, rec.Code)
			recipeRepository.AssertCalled(t, "Create", mock.IsType(recipe.Recipe{}))
		})
	})

	t.Run("PUT /recipes/:id", func(t *testing.T) {
		accessTest.ShouldUseAuthMiddleware(t, "PUT", "/recipes/1")
		accessTest.ShouldCheckPermissions(t, "PUT", "/recipes/1")

		t.Run("should allow updating a recipe", func(t *testing.T) {
			// Given
			setup := setupTest()
			recipeRepository := setup.recipeRepository
			ingredientRepository := setup.ingredientRepository
			mealTypeRepository := setup.mealTypeRepository
			server := setup.server

			mockIngredient := mockingredient.NewWithDefaults()
			mockIngredient.ID = uuid.MustParse("10000000-0000-0000-0000-000000000000")
			ingredientRepository.On("FindByIDs", mock.Anything).
				Return([]ingredient.Ingredient{mockIngredient}, nil)

			mockMealType := mockmealtype.NewWithDefaults()
			mockMealType.ID = uuid.MustParse("20000000-0000-0000-0000-000000000000")
			mealTypeRepository.On("FindByIDs", mock.Anything).
				Return([]mealtype.MealType{mockMealType}, nil)

			recipeRepository.On("Update", mock.Anything).Return(nil)

			payload := `
			{
				"name": "x",
				"imageUrl": "y",
				"timeToPrepare": 30,
				"enabled": true,
				"ingredients": [
					{
						"ingredientId": "10000000-0000-0000-0000-000000000000",
						"amount": 1
					}
				],
				"instructions": ["test"],
				"mealTypes": ["20000000-0000-0000-0000-000000000000"]
			}`
			recipeID := uuid.New()
			req := apitest.NewAuthRequestWithUser(user, "PUT", fmt.Sprintf("/recipes/%s", recipeID), []byte(payload))
			rec := httptest.NewRecorder()

			// When
			server.ServeHTTP(rec, req)

			// Then
			assert.Equal(t, http.StatusNoContent, rec.Code)
			recipeRepository.AssertCalled(t, "Update", mock.IsType(recipe.Recipe{}))
		})
	})
}
