package controllers

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/nutrimate/microservice-kit/pkg/api"
	"gitlab.com/nutrimate/microservice-kit/pkg/auth"

	"gitlab.com/nutrimate/cookbook/pkg/mealtype"
)

type mealTypesController struct {
	mealTypeRepository mealtype.Repository
}

func (controller mealTypesController) getAll(c echo.Context) error {
	mealTypes, err := controller.mealTypeRepository.FindAll()
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, mealTypes)
}

// Register adds handlers for /meal-types routes
func (controller mealTypesController) Register(e *echo.Echo, m api.ControllerMiddlewares) {
	mt := e.Group("/meal-types")
	mt.Use(m.Auth())
	mt.Use(auth.NewPermissionsMiddleware(auth.Permissions{
		All: []string{WriteCookbookScope},
	}))
	{
		mt.GET("", controller.getAll)
	}
}

// NewMealTypesController creates new controller for meal types routes
func NewMealTypesController(mealTypeRepository mealtype.Repository) api.Controller {
	return mealTypesController{
		mealTypeRepository: mealTypeRepository,
	}
}
