package controllers_test

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	"gitlab.com/nutrimate/microservice-kit/pkg/apitest"
	"gitlab.com/nutrimate/microservice-kit/pkg/auth"

	"gitlab.com/nutrimate/cookbook/internal/api/controllers"
	"gitlab.com/nutrimate/cookbook/pkg/mealtype"
	mockmealtype "gitlab.com/nutrimate/cookbook/pkg/mealtype/mock"
)

func TestNewMealTypesController(t *testing.T) {
	type testSetup struct {
		server             *echo.Echo
		mealTypeRepository *mockmealtype.Repository
	}

	setupTest := func() testSetup {
		mealTypeRepository := &mockmealtype.Repository{}
		controller := controllers.NewMealTypesController(mealTypeRepository)

		server := apitest.SetupTestAPIServer(controller)

		return testSetup{
			server:             server,
			mealTypeRepository: mealTypeRepository,
		}
	}

	accessTest := apitest.NewAccessTest(func() *echo.Echo {
		return setupTest().server
	})
	user := auth.RequestUser{
		ID: "1",
		Permissions: []string{
			controllers.WriteCookbookScope,
		},
		Scope: "",
	}

	t.Run("GET /meal-types", func(t *testing.T) {
		accessTest.ShouldUseAuthMiddleware(t, "GET", "/meal-types")
		accessTest.ShouldUseAuthMiddleware(t, "GET", "/meal-types")

		t.Run("should respond with all meal types", func(t *testing.T) {
			// Given
			setup := setupTest()
			mealTypeRepository := setup.mealTypeRepository
			server := setup.server

			mealTypes := []mealtype.MealType{
				mockmealtype.NewWithDefaults(),
				mockmealtype.NewWithDefaults(),
				mockmealtype.NewWithDefaults(),
			}
			mealTypeRepository.On("FindAll").Return(mealTypes, nil)

			req := apitest.NewAuthRequestWithUser(user, "GET", "/meal-types", nil)
			rec := httptest.NewRecorder()

			// When
			server.ServeHTTP(rec, req)

			// Then
			var bodyItems []mealtype.MealType

			assert.Equal(t, http.StatusOK, rec.Code)
			if err := json.Unmarshal(rec.Body.Bytes(), &bodyItems); !assert.NoError(t, err) {
				assert.Equal(t, mealTypes, bodyItems)
			}
		})
	})
}
