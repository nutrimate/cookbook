package controllers

// WriteCookbookScope is a scope which allows write access to cookbook
const WriteCookbookScope = "write:cookbook"
