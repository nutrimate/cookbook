package controllers

import (
	"errors"
	"net/http"

	"github.com/labstack/echo/v4"

	"gitlab.com/nutrimate/cookbook/pkg/shared"
)

// DomainValidationErrorToHTTP maps the shared validation error to http.StatusBadRequest code and otherwise returns original error
func DomainValidationErrorToHTTP(err error) error {
	if errors.Is(err, shared.ErrValidation) {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}

	return err
}
