package controllers

import (
	"net/http"

	"github.com/google/uuid"
	"github.com/labstack/echo/v4"
	"gitlab.com/nutrimate/microservice-kit/pkg/api"
	"gitlab.com/nutrimate/microservice-kit/pkg/auth"

	"gitlab.com/nutrimate/cookbook/internal/api/controllers/requests"
	"gitlab.com/nutrimate/cookbook/internal/api/controllers/responses"
	"gitlab.com/nutrimate/cookbook/pkg/ingredient"
	"gitlab.com/nutrimate/cookbook/pkg/mealtype"
	"gitlab.com/nutrimate/cookbook/pkg/recipe"
)

type recipesController struct {
	recipeRepository     recipe.Repository
	ingredientRepository ingredient.Repository
	mealTypeRepository   mealtype.Repository
}

func (controller recipesController) getAll(c echo.Context) error {
	recipes, err := controller.recipeRepository.FindAll()
	if err != nil {
		return err
	}

	compact := c.QueryParam("compact")
	if compact == "true" {
		compactRecipes := responses.MapRecipesToCompactRecipes(recipes)
		return c.JSON(http.StatusOK, compactRecipes)
	}
	return c.JSON(http.StatusOK, recipes)
}

func (controller recipesController) create(c echo.Context) error {
	var payload requests.CreateRecipePayload
	err := c.Bind(&payload)
	if err != nil {
		return err
	}

	builder, err := requests.MapCreateRecipePayloadToRecipeBuilder(controller.ingredientRepository, controller.mealTypeRepository, payload)
	if err != nil {
		return err
	}

	r, err := recipe.New(builder)
	if err != nil {
		return DomainValidationErrorToHTTP(err)
	}

	err = controller.recipeRepository.Create(r)
	if err != nil {
		return err
	}

	return c.NoContent(http.StatusCreated)
}

func (controller recipesController) update(c echo.Context) error {
	var payload requests.UpdateRecipePayload
	paramID := c.Param("id")
	id, err := uuid.Parse(paramID)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, "invalid recipe id")
	}

	err = c.Bind(&payload)
	if err != nil {
		return err
	}

	builder, err := requests.MapUpdateRecipePayloadToRecipeBuilder(
		controller.ingredientRepository,
		controller.mealTypeRepository,
		payload,
		id,
	)
	if err != nil {
		return err
	}

	r, err := recipe.New(builder)
	if err != nil {
		return DomainValidationErrorToHTTP(err)
	}

	err = controller.recipeRepository.Update(r)
	if err != nil {
		return err
	}

	return c.NoContent(http.StatusNoContent)
}

// Register adds handlers for /recipes routes
func (controller recipesController) Register(e *echo.Echo, m api.ControllerMiddlewares) {
	r := e.Group("/recipes")
	r.Use(m.Auth())
	r.Use(auth.NewPermissionsMiddleware(auth.Permissions{
		All: []string{WriteCookbookScope},
	}))
	{
		r.GET("", controller.getAll)
		r.POST("", controller.create)
		r.PUT("/:id", controller.update)
	}
}

// NewRecipesController creates new controller for recipes routes
func NewRecipesController(
	recipeRepository recipe.Repository,
	ingredientRepository ingredient.Repository,
	mealTypeRepository mealtype.Repository,
) api.Controller {
	return recipesController{
		recipeRepository:     recipeRepository,
		ingredientRepository: ingredientRepository,
		mealTypeRepository:   mealTypeRepository,
	}
}
