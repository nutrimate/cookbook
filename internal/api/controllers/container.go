package controllers

import (
	"gitlab.com/nutrimate/microservice-kit/pkg/api"

	"gitlab.com/nutrimate/cookbook/internal/repositories"
)

// Container is a container module that provides controllers
type Container interface {
	GetHealthCheckController() api.Controller
	GetIngredientsController() api.Controller
	GetMealTypesController() api.Controller
	GetRecipesController() api.Controller
	GetUnitsController() api.Controller
}

type container struct {
	repositories repositories.Container
}

// GetHealthCheckController returns new health check controller
func (c container) GetHealthCheckController() api.Controller {
	return NewHealthCheckController()
}

// GetIngredientsController returns new ingredients controller
func (c container) GetIngredientsController() api.Controller {
	ingredientRepository := c.repositories.GetIngredientRepository()
	unitRepository := c.repositories.GetUnitRepository()

	return NewIngredientsController(ingredientRepository, unitRepository)
}

// GetMealTypesController returns new meal types controller
func (c container) GetMealTypesController() api.Controller {
	mealTypeRepository := c.repositories.GetMealTypeRepository()

	return NewMealTypesController(mealTypeRepository)
}

// GetRecipesController returns new recipes controller
func (c container) GetRecipesController() api.Controller {
	recipeRepository := c.repositories.GetRecipeRepository()
	ingredientRepository := c.repositories.GetIngredientRepository()
	mealTypeRepository := c.repositories.GetMealTypeRepository()

	return NewRecipesController(recipeRepository, ingredientRepository, mealTypeRepository)
}

// GetUnitsController returns new units controller
func (c container) GetUnitsController() api.Controller {
	unitRepository := c.repositories.GetUnitRepository()

	return NewUnitsController(unitRepository)
}

// NewContainer creates a new container with controllers
func NewContainer(r repositories.Container) Container {
	return container{
		repositories: r,
	}
}
