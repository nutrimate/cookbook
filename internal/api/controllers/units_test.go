package controllers_test

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/google/uuid"
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/nutrimate/microservice-kit/pkg/apitest"
	"gitlab.com/nutrimate/microservice-kit/pkg/auth"

	"gitlab.com/nutrimate/cookbook/internal/api/controllers"
	"gitlab.com/nutrimate/cookbook/pkg/unit"
	mockunit "gitlab.com/nutrimate/cookbook/pkg/unit/mock"
)

func TestNewUnitsController(t *testing.T) {
	type testSetup struct {
		server         *echo.Echo
		unitRepository *mockunit.Repository
	}

	setupTest := func() testSetup {
		unitRepository := &mockunit.Repository{}

		controller := controllers.NewUnitsController(unitRepository)

		server := apitest.SetupTestAPIServer(controller)

		return testSetup{
			server:         server,
			unitRepository: unitRepository,
		}
	}

	accessTest := apitest.NewAccessTest(func() *echo.Echo {
		return setupTest().server
	})
	user := auth.RequestUser{
		ID: "1",
		Permissions: []string{
			controllers.WriteCookbookScope,
		},
		Scope: "",
	}

	t.Run("GET /units", func(t *testing.T) {
		accessTest.ShouldUseAuthMiddleware(t, "GET", "/units")
		accessTest.ShouldCheckPermissions(t, "GET", "/units")

		t.Run("should respond with all units", func(t *testing.T) {
			// Given
			setup := setupTest()
			unitRepository := setup.unitRepository
			server := setup.server

			units := []unit.Unit{
				mockunit.NewWithDefaults(),
				mockunit.NewWithDefaults(),
				mockunit.NewWithDefaults(),
			}
			unitRepository.On("FindAll").Return(units, nil)

			req := apitest.NewAuthRequestWithUser(user, "GET", "/units", nil)
			rec := httptest.NewRecorder()

			// When
			server.ServeHTTP(rec, req)

			// Then
			var bodyItems []unit.Unit

			assert.Equal(t, http.StatusOK, rec.Code)
			if err := json.Unmarshal(rec.Body.Bytes(), &bodyItems); !assert.NoError(t, err) {
				assert.Equal(t, units, bodyItems)
			}
		})
	})

	t.Run("POST /units", func(t *testing.T) {
		accessTest.ShouldUseAuthMiddleware(t, "POST", "/units")
		accessTest.ShouldCheckPermissions(t, "POST", "/units")

		t.Run("should allow creating new unit", func(t *testing.T) {
			// Given
			setup := setupTest()
			unitRepository := setup.unitRepository
			server := setup.server

			unitRepository.On("Create", mock.Anything).Return(nil)

			payload := `{ "id":"00000000-0000-0000-0000-000000000000", "name": "g", "plurals": {"1":"g"} }`
			req := apitest.NewAuthRequestWithUser(user, "POST", "/units", []byte(payload))
			rec := httptest.NewRecorder()

			// When
			server.ServeHTTP(rec, req)

			// Then
			assert.Equal(t, http.StatusCreated, rec.Code)
			unitRepository.AssertCalled(t, "Create", mock.IsType(unit.Unit{}))
		})
	})

	t.Run("PUT /units/:id", func(t *testing.T) {
		accessTest.ShouldUseAuthMiddleware(t, "PUT", "/units/1")
		accessTest.ShouldCheckPermissions(t, "PUT", "/units/1")

		t.Run("should allow updating a unit", func(t *testing.T) {
			// Given
			setup := setupTest()
			unitRepository := setup.unitRepository
			server := setup.server

			unitRepository.On("Update", mock.Anything).Return(nil)

			payload := `{ "name": "g", "plurals": {"1":"g"} }`
			unitID := uuid.New()
			req := apitest.NewAuthRequestWithUser(user, "PUT", fmt.Sprintf("/units/%s", unitID), []byte(payload))
			rec := httptest.NewRecorder()

			// When
			server.ServeHTTP(rec, req)

			// Then
			assert.Equal(t, http.StatusNoContent, rec.Code)
			unitRepository.AssertCalled(t, "Update", mock.IsType(unit.Unit{}))
		})
	})
}
