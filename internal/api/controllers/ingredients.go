package controllers

import (
	"net/http"

	"gitlab.com/nutrimate/microservice-kit/pkg/api"
	"gitlab.com/nutrimate/microservice-kit/pkg/auth"

	"gitlab.com/nutrimate/cookbook/internal/api/controllers/requests"

	"github.com/google/uuid"
	"github.com/labstack/echo/v4"

	"gitlab.com/nutrimate/cookbook/pkg/ingredient"
	"gitlab.com/nutrimate/cookbook/pkg/unit"
)

type ingredientsController struct {
	ingredientRepository ingredient.Repository
	unitRepository       unit.Repository
}

func (controller ingredientsController) getAll(c echo.Context) error {
	ingredients, err := controller.ingredientRepository.FindAll()
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, ingredients)
}

func (controller ingredientsController) create(c echo.Context) error {
	var payload requests.CreateIngredientPayload
	err := c.Bind(&payload)
	if err != nil {
		return err
	}

	builder, err := requests.MapCreateIngredientPayloadToIngredientBuilder(controller.unitRepository, payload)
	if err != nil {
		return err
	}

	createdUnit, err := ingredient.New(builder)
	if err != nil {
		return DomainValidationErrorToHTTP(err)
	}

	err = controller.ingredientRepository.Create(createdUnit)
	if err != nil {
		return err
	}

	return c.NoContent(http.StatusCreated)
}

func (controller ingredientsController) update(c echo.Context) error {
	paramID := c.Param("id")
	id, err := uuid.Parse(paramID)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, "invalid ingredient id")
	}

	var payload requests.UpdateIngredientPayload
	err = c.Bind(&payload)
	if err != nil {
		return err
	}

	builder, err := requests.MapUpdateIngredientPayloadToIngredientBuilder(controller.unitRepository, payload, id)
	if err != nil {
		return err
	}

	createdUnit, err := ingredient.New(builder)
	if err != nil {
		return DomainValidationErrorToHTTP(err)
	}

	err = controller.ingredientRepository.Update(createdUnit)
	if err != nil {
		return err
	}

	return c.NoContent(http.StatusNoContent)
}

// Register adds handlers /ingredients routes
func (controller ingredientsController) Register(e *echo.Echo, m api.ControllerMiddlewares) {
	i := e.Group("/ingredients")
	i.Use(m.Auth())
	i.Use(auth.NewPermissionsMiddleware(auth.Permissions{
		All: []string{WriteCookbookScope},
	}))
	{
		i.GET("", controller.getAll)
		i.POST("", controller.create)
		i.PUT("/:id", controller.update)
	}
}

// NewIngredientsController creates new controller for ingredient routes
func NewIngredientsController(ingredientRepository ingredient.Repository, unitRepository unit.Repository) api.Controller {
	return ingredientsController{
		ingredientRepository: ingredientRepository,
		unitRepository:       unitRepository,
	}
}
