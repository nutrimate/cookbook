package controllers_test

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	"gitlab.com/nutrimate/microservice-kit/pkg/apitest"

	"gitlab.com/nutrimate/cookbook/internal/api/controllers"
)

type healthCheckControllerTestSetup struct {
	server *echo.Echo
}

func setupHealthCheckControllerTest() healthCheckControllerTestSetup {
	controller := controllers.NewHealthCheckController()
	server := apitest.SetupTestAPIServer(controller)

	return healthCheckControllerTestSetup{
		server,
	}
}

func TestNewHealthCheckController(t *testing.T) {
	t.Run("GET /health", func(t *testing.T) {
		tests := []struct {
			name           string
			expectedStatus int
		}{
			{
				name:           "should return ok status",
				expectedStatus: http.StatusOK,
			},
		}

		for _, testCase := range tests {
			t.Run(testCase.name, func(t *testing.T) {
				// Given
				setup := setupHealthCheckControllerTest()
				server := setup.server

				rec := httptest.NewRecorder()
				req := apitest.NewRequest("GET", "/health", nil)

				// When
				server.ServeHTTP(rec, req)

				// Then
				assert.Equal(t, testCase.expectedStatus, rec.Code)
			})
		}
	})
}
