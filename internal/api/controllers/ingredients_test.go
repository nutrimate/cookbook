package controllers_test

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/google/uuid"
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/nutrimate/microservice-kit/pkg/apitest"
	"gitlab.com/nutrimate/microservice-kit/pkg/auth"

	"gitlab.com/nutrimate/cookbook/internal/api/controllers"
	"gitlab.com/nutrimate/cookbook/pkg/ingredient"
	mockingredient "gitlab.com/nutrimate/cookbook/pkg/ingredient/mock"
	mockunit "gitlab.com/nutrimate/cookbook/pkg/unit/mock"
)

func TestNewIngredientsController(t *testing.T) {
	type testSetup struct {
		server               *echo.Echo
		ingredientRepository *mockingredient.Repository
		unitRepository       *mockunit.Repository
	}

	setupTest := func() testSetup {
		ingredientRepository := &mockingredient.Repository{}
		unitRepository := &mockunit.Repository{}
		controller := controllers.NewIngredientsController(ingredientRepository, unitRepository)

		server := apitest.SetupTestAPIServer(controller)

		return testSetup{
			server:               server,
			ingredientRepository: ingredientRepository,
			unitRepository:       unitRepository,
		}
	}

	accessTest := apitest.NewAccessTest(func() *echo.Echo {
		return setupTest().server
	})
	user := auth.RequestUser{
		ID: "1",
		Permissions: []string{
			controllers.WriteCookbookScope,
		},
		Scope: "",
	}

	t.Run("GET /ingredients", func(t *testing.T) {
		accessTest.ShouldUseAuthMiddleware(t, "GET", "/ingredients")
		accessTest.ShouldCheckPermissions(t, "GET", "/ingredients")

		t.Run("should respond with all ingredients", func(t *testing.T) {
			// Given
			setup := setupTest()
			ingredientRepository := setup.ingredientRepository
			server := setup.server

			ingredients := []ingredient.Ingredient{
				mockingredient.NewWithDefaults(),
				mockingredient.NewWithDefaults(),
				mockingredient.NewWithDefaults(),
			}
			ingredientRepository.On("FindAll").Return(ingredients, nil)

			req := apitest.NewAuthRequestWithUser(user, "GET", "/ingredients", nil)
			rec := httptest.NewRecorder()

			// When
			server.ServeHTTP(rec, req)

			// Then
			var bodyItems []ingredient.Ingredient

			assert.Equal(t, http.StatusOK, rec.Code)
			if err := json.Unmarshal(rec.Body.Bytes(), &bodyItems); !assert.NoError(t, err) {
				assert.Equal(t, ingredients, bodyItems)
			}
		})
	})

	t.Run("POST /ingredients", func(t *testing.T) {
		accessTest.ShouldUseAuthMiddleware(t, "POST", "/ingredients")

		t.Run("should allow creating new ingredient", func(t *testing.T) {
			// Given
			setup := setupTest()
			unitRepository := setup.unitRepository
			ingredientRepository := setup.ingredientRepository
			server := setup.server

			unitRepository.On("FindByID", mock.Anything).Return(mockunit.NewWithDefaults(), nil)
			ingredientRepository.On("Create", mock.Anything).Return(nil)

			payload := `
			{
				"id":"00000000-0000-0000-0000-000000000000",
				"name": "x",
				"unitId": "00000000-0000-0000-0000-000000000000",
				"unitAmount": 1,
				"calories": 0,
				"fats": 0,
				"proteins": 0,
				"carbohydrates": 0
			}`

			req := apitest.NewAuthRequestWithUser(user, "POST", "/ingredients", []byte(payload))
			rec := httptest.NewRecorder()

			// When
			server.ServeHTTP(rec, req)

			// Then
			assert.Equal(t, http.StatusCreated, rec.Code)
			ingredientRepository.AssertCalled(t, "Create", mock.IsType(ingredient.Ingredient{}))
		})
	})

	t.Run("PUT /ingredients/:id", func(t *testing.T) {
		accessTest.ShouldUseAuthMiddleware(t, "PUT", "/ingredients/1")

		t.Run("should allow updating ingredients", func(t *testing.T) {
			// Given
			setup := setupTest()
			unitRepository := setup.unitRepository
			ingredientRepository := setup.ingredientRepository
			server := setup.server

			unitRepository.On("FindByID", mock.Anything).Return(mockunit.NewWithDefaults(), nil)
			ingredientRepository.On("Update", mock.Anything).Return(nil)

			id := uuid.MustParse("00000000-0000-0000-0000-000000000000")
			payload := `
			{
				"name": "x",
				"unitId": "00000000-0000-0000-0000-000000000000",
				"unitAmount": 1,
				"calories": 0,
				"fats": 0,
				"proteins": 0,
				"carbohydrates": 0
			}`

			req := apitest.NewAuthRequestWithUser(user, "PUT", fmt.Sprintf("/ingredients/%s", id.String()), []byte(payload))
			rec := httptest.NewRecorder()

			// When
			server.ServeHTTP(rec, req)

			// Then
			assert.Equal(t, http.StatusNoContent, rec.Code)
		})
	})
}
