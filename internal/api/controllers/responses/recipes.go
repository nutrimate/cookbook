package responses

import (
	"github.com/google/uuid"

	"gitlab.com/nutrimate/cookbook/pkg/recipe"
)

// CompactRecipe represents a recipe.Recipe with aggregated information
type CompactRecipe struct {
	ID            uuid.UUID   `json:"id"`
	MealTypeIDs   []uuid.UUID `json:"mealTypeIds"`
	Calories      float64     `json:"calories"`
	Proteins      float64     `json:"proteins"`
	Fats          float64     `json:"fats"`
	Carbohydrates float64     `json:"carbohydrates"`
	Ingredients   []uuid.UUID `json:"ingredients"`
}

// MapRecipesToCompactRecipes maps domain recipes to their compact form
func MapRecipesToCompactRecipes(recipes []recipe.Recipe) []CompactRecipe {
	compactRecipes := make([]CompactRecipe, len(recipes))

	for i, r := range recipes {
		mealTypeIDs := make([]uuid.UUID, len(r.MealTypes))
		for i, mt := range r.MealTypes {
			mealTypeIDs[i] = mt.ID
		}

		calories := 0.0
		proteins := 0.0
		fats := 0.0
		carbohydrates := 0.0
		ingredientsIds := []uuid.UUID{}

		for _, i := range r.Ingredients {
			ingredient := i.Ingredient
			factor := float64(i.Amount) / float64(ingredient.UnitAmount)

			calories += factor * float64(ingredient.Calories)
			proteins += factor * float64(ingredient.Proteins)
			fats += factor * float64(ingredient.Fats)
			carbohydrates += factor * float64(ingredient.Carbohydrates)
			ingredientsIds = append(ingredientsIds, ingredient.ID)
		}

		compactRecipes[i] = CompactRecipe{
			ID:            r.ID,
			MealTypeIDs:   mealTypeIDs,
			Calories:      calories,
			Proteins:      proteins,
			Fats:          fats,
			Carbohydrates: carbohydrates,
			Ingredients:   ingredientsIds,
		}
	}

	return compactRecipes
}
