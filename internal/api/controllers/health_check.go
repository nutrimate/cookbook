package controllers

import (
	"github.com/etherlabsio/healthcheck"
	"github.com/labstack/echo/v4"
	"gitlab.com/nutrimate/microservice-kit/pkg/api"
)

type healthCheckController struct {
}

func (controller healthCheckController) checkHealth(c echo.Context) error {
	handler := healthcheck.HandlerFunc()

	return echo.WrapHandler(handler)(c)
}

// Register add handlers for /health routes
func (controller healthCheckController) Register(e *echo.Echo, _ api.ControllerMiddlewares) {
	health := e.Group("/health")
	{
		health.GET("", controller.checkHealth)
	}
}

// NewHealthCheckController creates a new controller for /health routes
func NewHealthCheckController() api.Controller {
	return healthCheckController{}
}
