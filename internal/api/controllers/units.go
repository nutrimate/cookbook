package controllers

import (
	"net/http"

	"gitlab.com/nutrimate/microservice-kit/pkg/api"
	"gitlab.com/nutrimate/microservice-kit/pkg/auth"

	"gitlab.com/nutrimate/cookbook/internal/api/controllers/requests"

	"github.com/google/uuid"
	"github.com/labstack/echo/v4"

	"gitlab.com/nutrimate/cookbook/pkg/unit"
)

type unitsController struct {
	unitRepository unit.Repository
}

func (controller unitsController) getAll(c echo.Context) error {
	units, err := controller.unitRepository.FindAll()
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, units)
}

func (controller unitsController) create(c echo.Context) error {
	var payload requests.CreateUnitPayload
	err := c.Bind(&payload)
	if err != nil {
		return err
	}

	u, err := unit.New(*payload.ID, *payload.Name, payload.Plurals)
	if err != nil {
		return DomainValidationErrorToHTTP(err)
	}

	err = controller.unitRepository.Create(u)
	if err != nil {
		return err
	}

	return c.NoContent(http.StatusCreated)
}

func (controller unitsController) update(c echo.Context) error {
	paramID := c.Param("id")
	id, err := uuid.Parse(paramID)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, "invalid unit id")
	}
	var payload requests.UpdateUnitPayload
	err = c.Bind(&payload)
	if err != nil {
		return err
	}

	u, err := unit.New(id, *payload.Name, payload.Plurals)
	if err != nil {
		return DomainValidationErrorToHTTP(err)
	}

	err = controller.unitRepository.Update(u)
	if err != nil {
		return err
	}

	return c.NoContent(http.StatusNoContent)
}

// Register adds handler for /units routes
func (controller unitsController) Register(e *echo.Echo, m api.ControllerMiddlewares) {
	u := e.Group("/units")
	u.Use(m.Auth())
	u.Use(auth.NewPermissionsMiddleware(auth.Permissions{
		All: []string{WriteCookbookScope},
	}))
	{
		u.GET("", controller.getAll)
		u.POST("", controller.create)
		u.PUT("/:id", controller.update)
	}
}

// NewUnitsController creates new controller for units routes
func NewUnitsController(unitRepository unit.Repository) api.Controller {
	return unitsController{
		unitRepository: unitRepository,
	}
}
