package api

import (
	"fmt"

	"github.com/labstack/echo/v4"
	"github.com/upper/db/v4"
	postgres "github.com/upper/db/v4/adapter/postgresql"

	"gitlab.com/nutrimate/cookbook/internal/api/controllers"
	"gitlab.com/nutrimate/cookbook/internal/config"
	"gitlab.com/nutrimate/cookbook/internal/repositories"
)

// Container is a container module that provides API server components
type Container interface {
	GetConfig() config.Config
	GetServer() *echo.Echo
}

type container struct {
	config      config.Config
	controllers controllers.Container
}

// GetServer returns new API server
func (c container) GetServer() *echo.Echo {
	return NewServer(c.config, c.controllers)
}

// GetConfig returns app config
func (c container) GetConfig() config.Config {
	return c.config
}

// NewContainer creates a container for starting api server
func NewContainer() (Container, error) {
	apiConfig, err := config.Load()
	if err != nil {
		return container{}, fmt.Errorf("could not load config: %w", err)
	}

	dbSession, err := openDbSession(apiConfig)
	if err != nil {
		return container{}, err
	}

	repositoriesContainer := repositories.NewContainer(dbSession)
	controllersContainer := controllers.NewContainer(repositoriesContainer)

	c := container{
		config:      apiConfig,
		controllers: controllersContainer,
	}

	return c, nil
}

func openDbSession(config config.Config) (db.Session, error) {
	connectionURL, err := postgres.ParseURL(config.DbConnectionString)
	if err != nil {
		return nil, fmt.Errorf("invalid db connection string: %w", err)
	}

	session, err := postgres.Open(connectionURL)
	if err != nil {
		return nil, fmt.Errorf("could not open database connection: %w", err)
	}

	return session, nil
}
