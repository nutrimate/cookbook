package repositories

import (
	"fmt"

	"github.com/google/uuid"
	"github.com/upper/db/v4"

	"gitlab.com/nutrimate/cookbook/internal/repositories/dbmodels"
	"gitlab.com/nutrimate/cookbook/pkg/recipe"
)

type recipeDbRepository struct {
	session db.Session
}

func mergeRecipesDependencies(
	recipes []dbmodels.Recipe,
	instructions []dbmodels.RecipeInstruction,
	ingredients []dbmodels.RecipeIngredientPreloaded,
	mealTypes []dbmodels.RecipeMealTypePreloaded,
) []dbmodels.Recipe {
	recipeMap := map[uuid.UUID]*dbmodels.Recipe{}

	for index, r := range recipes {
		recipeMap[r.ID] = &recipes[index]
	}

	for _, i := range ingredients {
		r, ok := recipeMap[i.RecipeID]
		if !ok {
			continue
		}

		r.Ingredients = append(r.Ingredients, i)
	}

	for _, i := range instructions {
		r, ok := recipeMap[i.RecipeID]
		if !ok {
			continue
		}

		r.Instructions = append(r.Instructions, i)
	}

	for _, mt := range mealTypes {
		r, ok := recipeMap[mt.RecipeID]
		if !ok {
			continue
		}

		r.MealTypes = append(r.MealTypes, mt)
	}

	mergedRecipes := make([]dbmodels.Recipe, len(recipeMap))
	index := 0
	for _, r := range recipeMap {
		mergedRecipes[index] = *r
		index++
	}

	return mergedRecipes
}

// FindAll returns all meal types found in the database
func (r recipeDbRepository) FindAll() ([]recipe.Recipe, error) {
	var dbRecipes []dbmodels.Recipe
	err := r.session.
		Collection("recipes").
		Find().
		All(&dbRecipes)
	if err != nil {
		return nil, fmt.Errorf("failed to load recipes from database: %w", err)
	}

	var dbRecipeInstructions []dbmodels.RecipeInstruction
	err = r.session.
		Collection("recipe_instructions").
		Find().
		All(&dbRecipeInstructions)

	if err != nil {
		return nil, fmt.Errorf("failed to load instructions of recipes: %w", err)
	}

	var dbRecipeIngredients []dbmodels.RecipeIngredientPreloaded
	err = r.session.SQL().
		Select("i.id AS ingredient_id", "i.name AS ingredient_name", "u.name AS unit_name", "*").
		From("recipe_ingredients AS ri").
		Join("ingredients AS i").On("ri.ingredient_id = i.id").
		Join("units AS u").On("i.unit_id = u.id").
		All(&dbRecipeIngredients)

	if err != nil {
		return nil, fmt.Errorf("failed to load ingredients of recipes: %w", err)
	}

	var dbRecipeMealTypes []dbmodels.RecipeMealTypePreloaded
	err = r.session.SQL().
		Select("*").
		From("recipe_meal_types AS rmt").
		Join("meal_types AS mt").On("mt.id = rmt.meal_type_id").
		All(&dbRecipeMealTypes)

	if err != nil {
		return nil, fmt.Errorf("failed to load meal types of recipes: %w", err)
	}

	mergedRecipes := mergeRecipesDependencies(dbRecipes, dbRecipeInstructions, dbRecipeIngredients, dbRecipeMealTypes)
	recipes := []recipe.Recipe{}

	for _, dbRecipe := range mergedRecipes {
		r, err := dbRecipe.ToDomain()

		if err != nil {
			return nil, fmt.Errorf("failed to map database recipe to domain: %w", err)
		}

		recipes = append(recipes, r)
	}

	return recipes, nil
}

func saveRecipeRelationships(session db.Session, recipe recipe.Recipe) error {
	instructions := dbmodels.NewRecipeInstructionsFromDomain(recipe)
	insert := session.SQL().InsertInto("recipe_instructions")
	for _, i := range instructions {
		insert = insert.Values(i)
	}
	_, err := insert.Exec()
	if err != nil {
		return fmt.Errorf("failed to save recipe instructions: %w", err)
	}

	ingredients := dbmodels.NewRecipeIngredientsFromDomain(recipe)
	insert = session.SQL().InsertInto("recipe_ingredients")
	for _, i := range ingredients {
		insert = insert.Values(i)
	}
	_, err = insert.Exec()
	if err != nil {
		return fmt.Errorf("failed to save recipe ingredients: %w", err)
	}

	mealTypes := dbmodels.NewRecipeMealTypesFromDomain(recipe)
	insert = session.SQL().InsertInto("recipe_meal_types")
	for _, mt := range mealTypes {
		insert = insert.Values(mt)
	}
	_, err = insert.Exec()
	if err != nil {
		return fmt.Errorf("failed to save recipe meal types: %w", err)
	}

	return nil
}

// Create creates a new recipe
func (r recipeDbRepository) Create(recipe recipe.Recipe) error {
	err := r.session.Tx(func(session db.Session) error {
		dbRecipe := dbmodels.NewRecipeFromDomain(recipe)
		_, err := session.Collection("recipes").Insert(dbRecipe)
		if err != nil {
			return fmt.Errorf("failed to save recipe: %w", err)
		}

		return saveRecipeRelationships(session, recipe)
	})

	return err
}

// Update updates existing recipe
func (r recipeDbRepository) Update(recipe recipe.Recipe) error {
	err := r.session.Tx(func(session db.Session) error {
		dbRecipe := dbmodels.NewRecipeFromDomain(recipe)
		err := session.Collection("recipes").Find(dbRecipe.ID).Update(dbRecipe)
		if err != nil {
			return fmt.Errorf("failed to update recipe: %w", err)
		}

		cond := db.Cond{"recipe_id": recipe.ID}
		err = session.Collection("recipe_instructions").Find(cond).Delete()
		if err != nil {
			return fmt.Errorf("failed to delete previous recipe instructions: %w", err)
		}

		err = session.Collection("recipe_ingredients").Find(cond).Delete()
		if err != nil {
			return fmt.Errorf("failed to delete previous recipe ingredients: %w", err)
		}

		err = session.Collection("recipe_meal_types").Find(cond).Delete()
		if err != nil {
			return fmt.Errorf("failed to delete previous recipe meal types: %w", err)
		}

		return saveRecipeRelationships(session, recipe)
	})

	return err
}

// NewRecipeRepository creates database repository for recipes
func NewRecipeRepository(session db.Session) recipe.Repository {
	repository := &recipeDbRepository{
		session: session,
	}

	return repository
}
