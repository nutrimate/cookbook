package repositories

import "github.com/google/uuid"

// UniqueIDs returns input ids without duplicates
func UniqueIDs(ids []uuid.UUID) []uuid.UUID {
	if ids == nil {
		return nil
	}

	seenIDs := map[uuid.UUID]bool{}
	uniqueIDs := []uuid.UUID{}

	for _, id := range ids {
		if seenIDs[id] {
			continue
		}
		seenIDs[id] = true
		uniqueIDs = append(uniqueIDs, id)
	}

	return uniqueIDs
}
