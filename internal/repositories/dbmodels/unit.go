package dbmodels

import (
	"github.com/google/uuid"

	"gitlab.com/nutrimate/cookbook/pkg/unit"
)

// Unit is a database model of unit.Unit
type Unit struct {
	ID      uuid.UUID         `db:"id"`
	Name    string            `db:"name"`
	Plurals map[string]string `db:"plurals"`
}

// ToDomain maps database unit to domain unit
func (u Unit) ToDomain() (unit.Unit, error) {
	return unit.New(u.ID, u.Name, u.Plurals)
}

// NewUnitFromDomain maps domain unit to database unit
func NewUnitFromDomain(u unit.Unit) Unit {
	return Unit{
		ID:      u.ID,
		Name:    string(u.Name),
		Plurals: u.Plurals,
	}
}
