package dbmodels

import (
	"github.com/google/uuid"

	"gitlab.com/nutrimate/cookbook/pkg/mealtype"
)

// MealType is a database model of mealtype.MealType
type MealType struct {
	ID   uuid.UUID `db:"id"`
	Name string    `db:"name"`
}

// ToDomain maps database meal type to domain meal type
func (mt MealType) ToDomain() mealtype.MealType {
	return mealtype.MealType{
		ID:   mt.ID,
		Name: mt.Name,
	}
}
