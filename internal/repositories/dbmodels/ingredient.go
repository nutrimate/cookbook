package dbmodels

import (
	"github.com/google/uuid"

	"gitlab.com/nutrimate/cookbook/pkg/ingredient"
)

// Ingredient is a database model of ingredient.Ingredient
type Ingredient struct {
	ID            uuid.UUID `db:"id"`
	Name          string    `db:"name"`
	UnitID        uuid.UUID `db:"unit_id"`
	UnitAmount    float64   `db:"unit_amount"`
	Calories      float64   `db:"calories"`
	Proteins      float64   `db:"proteins"`
	Fats          float64   `db:"fats"`
	Carbohydrates float64   `db:"carbohydrates"`
}

// IngredientWithUnit represents an ingredient with unit joined together
type IngredientWithUnit struct {
	IngredientID   uuid.UUID  `db:"ingredient_id"`
	IngredientName string     `db:"ingredient_name"`
	UnitName       string     `db:"unit_name"`
	Ingredient     Ingredient `db:",inline"`
	Unit           Unit       `db:",inline"`
}

// ToDomain maps database ingredient to domain ingredient
func (i IngredientWithUnit) ToDomain() (ingredient.Ingredient, error) {
	// Assign ambiguous fields
	i.Ingredient.ID = i.IngredientID
	i.Ingredient.Name = i.IngredientName
	i.Unit.ID = i.Ingredient.UnitID
	i.Unit.Name = i.UnitName

	unit, err := i.Unit.ToDomain()
	if err != nil {
		return ingredient.Ingredient{}, err
	}

	builder := ingredient.Builder{
		ID:            i.Ingredient.ID,
		Name:          i.Ingredient.Name,
		Unit:          unit,
		UnitAmount:    i.Ingredient.UnitAmount,
		Calories:      i.Ingredient.Calories,
		Proteins:      i.Ingredient.Proteins,
		Fats:          i.Ingredient.Fats,
		Carbohydrates: i.Ingredient.Carbohydrates,
	}
	return ingredient.New(builder)
}

// NewIngredientFromDomain maps domain ingredient to database ingredient
func NewIngredientFromDomain(i ingredient.Ingredient) Ingredient {
	return Ingredient{
		ID:            i.ID,
		Name:          string(i.Name),
		UnitID:        i.Unit.ID,
		UnitAmount:    float64(i.UnitAmount),
		Calories:      float64(i.Calories),
		Proteins:      float64(i.Proteins),
		Fats:          float64(i.Fats),
		Carbohydrates: float64(i.Carbohydrates),
	}
}
