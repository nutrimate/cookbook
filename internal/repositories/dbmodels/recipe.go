package dbmodels

import (
	"github.com/google/uuid"

	"gitlab.com/nutrimate/cookbook/pkg/mealtype"
	"gitlab.com/nutrimate/cookbook/pkg/recipe"
)

// Recipe describes "recipes" table
type Recipe struct {
	ID            uuid.UUID                   `db:"id"`
	Name          string                      `db:"name"`
	ImageURL      *string                     `db:"image_url"`
	TimeToPrepare uint8                       `db:"time_to_prepare"`
	Enabled       bool                        `db:"enabled"`
	Instructions  []RecipeInstruction         `db:","`
	Ingredients   []RecipeIngredientPreloaded `db:","`
	MealTypes     []RecipeMealTypePreloaded   `db:","`
}

// ToDomain maps database model to domain model
func (r Recipe) ToDomain() (recipe.Recipe, error) {
	instructions := make([]recipe.InstructionBuilder, len(r.Instructions))
	for index, ri := range r.Instructions {
		instructions[index] = recipe.InstructionBuilder{
			Step:        ri.Step,
			Description: ri.Description,
		}
	}

	ingredients := make([]recipe.IngredientBuilder, len(r.Ingredients))
	for index, ri := range r.Ingredients {
		i, err := ri.Ingredient.ToDomain()
		if err != nil {
			return recipe.Recipe{}, err
		}

		ingredients[index] = recipe.IngredientBuilder{
			Amount:     ri.Amount,
			Ingredient: i,
		}
	}

	mealTypes := make([]mealtype.MealType, len(r.MealTypes))
	for index, rmt := range r.MealTypes {
		mealTypes[index] = rmt.MealType.ToDomain()
	}

	builder := recipe.Builder{
		ID:            r.ID,
		Name:          r.Name,
		ImageURL:      r.ImageURL,
		TimeToPrepare: r.TimeToPrepare,
		Enabled:       r.Enabled,
		Instructions:  instructions,
		Ingredients:   ingredients,
		MealTypes:     mealTypes,
	}

	return recipe.New(builder)
}

// NewRecipeFromDomain maps a domain recipe to database model
func NewRecipeFromDomain(r recipe.Recipe) Recipe {
	return Recipe{
		ID:            r.ID,
		Name:          string(r.Name),
		ImageURL:      r.ImageURL,
		TimeToPrepare: r.TimeToPrepare,
		Enabled:       r.Enabled,
	}
}

// RecipeInstruction describes "recipes_instruction" table
type RecipeInstruction struct {
	RecipeID    uuid.UUID `db:"recipe_id"`
	Step        uint8     `db:"step"`
	Description string    `db:"description"`
}

// NewRecipeInstructionsFromDomain creates RecipeInstruction models from domain recipe
func NewRecipeInstructionsFromDomain(r recipe.Recipe) []RecipeInstruction {
	instructions := make([]RecipeInstruction, len(r.Instructions))

	for index, i := range r.Instructions {
		instructions[index] = RecipeInstruction{
			RecipeID:    r.ID,
			Step:        i.Step,
			Description: string(i.Description),
		}
	}

	return instructions
}

// RecipeIngredient describes "recipe_ingredients" table
type RecipeIngredient struct {
	RecipeID     uuid.UUID `db:"recipe_id"`
	IngredientID uuid.UUID `db:"ingredient_id"`
	Amount       float64   `db:"amount"`
}

// RecipeIngredientPreloaded describes "recipe_ingredients" table with ingredients tables joined
type RecipeIngredientPreloaded struct {
	RecipeIngredient `db:",inline"`
	Ingredient       IngredientWithUnit `db:",inline"`
}

// NewRecipeIngredientsFromDomain creates RecipeIngredient models from domain recipe
func NewRecipeIngredientsFromDomain(r recipe.Recipe) []RecipeIngredient {
	ingredients := make([]RecipeIngredient, len(r.Ingredients))

	for index, i := range r.Ingredients {
		ingredients[index] = RecipeIngredient{
			RecipeID:     r.ID,
			IngredientID: i.Ingredient.ID,
			Amount:       float64(i.Amount),
		}
	}

	return ingredients
}

// RecipeMealType describe "recipe_meal_type" table
type RecipeMealType struct {
	RecipeID   uuid.UUID `db:"recipe_id"`
	MealTypeID uuid.UUID `db:"meal_type_id"`
}

// RecipeMealTypePreloaded describe "recipe_meal_type" table with "meal_type" table joined
type RecipeMealTypePreloaded struct {
	RecipeMealType `db:",inline"`
	MealType       MealType `db:",inline"`
}

// NewRecipeMealTypesFromDomain creates RecipeMealType models from domain recipe
func NewRecipeMealTypesFromDomain(r recipe.Recipe) []RecipeMealType {
	mealTypes := make([]RecipeMealType, len(r.MealTypes))

	for index, mt := range r.MealTypes {
		mealTypes[index] = RecipeMealType{
			RecipeID:   r.ID,
			MealTypeID: mt.ID,
		}
	}

	return mealTypes
}
