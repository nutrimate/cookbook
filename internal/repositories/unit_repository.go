package repositories

import (
	"fmt"

	"gitlab.com/nutrimate/cookbook/internal/repositories/dbmodels"

	"github.com/google/uuid"
	"github.com/upper/db/v4"

	"gitlab.com/nutrimate/cookbook/pkg/unit"
)

type unitDbRepository struct {
	units func() db.Collection
}

// FindAll returns all units in database
func (r unitDbRepository) FindAll() ([]unit.Unit, error) {
	var dbUnits []dbmodels.Unit

	err := r.units().Find().All(&dbUnits)
	if err != nil {
		return nil, fmt.Errorf("failed to load units from database: %w", err)
	}

	units := []unit.Unit{}
	for _, dbUnit := range dbUnits {
		u, err := dbUnit.ToDomain()
		if err != nil {
			return nil, fmt.Errorf("failed to map database unit to domain: %w", err)
		}

		units = append(units, u)
	}

	return units, nil
}

// FindByID find unit with provided id in the database and returns it
func (r unitDbRepository) FindByID(id uuid.UUID) (unit.Unit, error) {
	var dbUnit dbmodels.Unit

	err := r.units().Find(id).One(&dbUnit)
	if err != nil {
		return unit.Unit{}, fmt.Errorf("failed to get unit with id=%s from database: %w", id, err)
	}

	u, err := dbUnit.ToDomain()
	if err != nil {
		return unit.Unit{}, fmt.Errorf("failed to map database unit to domain: %w", err)
	}

	return u, nil
}

// Create creates new unit
func (r unitDbRepository) Create(u unit.Unit) error {
	dbUnit := dbmodels.NewUnitFromDomain(u)
	_, err := r.units().Insert(&dbUnit)
	if err != nil {
		return fmt.Errorf("failed to create unit: %w", err)
	}

	return nil
}

// Update updates existing unit
func (r unitDbRepository) Update(u unit.Unit) error {
	dbUnit := dbmodels.NewUnitFromDomain(u)
	err := r.units().Find(u.ID).Update(&dbUnit)

	if err != nil {
		return fmt.Errorf("failed to update unit: %w", err)
	}

	return nil
}

// NewUnitRepository creates database repository for units
func NewUnitRepository(session db.Session) unit.Repository {
	repository := &unitDbRepository{
		units: func() db.Collection {
			return session.Collection("units")
		},
	}

	return repository
}
