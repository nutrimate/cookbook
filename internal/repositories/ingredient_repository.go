package repositories

import (
	"fmt"

	"github.com/google/uuid"
	"github.com/upper/db/v4"

	"gitlab.com/nutrimate/cookbook/internal/repositories/dbmodels"
	"gitlab.com/nutrimate/cookbook/pkg/ingredient"
)

type ingredientDbRepository struct {
	session     db.Session
	ingredients func() db.Collection
}

func (r ingredientDbRepository) findIngredientsWithUnits() db.Selector {
	return r.session.SQL().
		Select("i.id AS ingredient_id", "i.name AS ingredient_name", "u.name AS unit_name", "*").
		From("ingredients AS i").
		Join("units AS u").On("u.id = i.unit_id")
}

// FindAll returns all ingredients found in the database
func (r ingredientDbRepository) FindAll() ([]ingredient.Ingredient, error) {
	var dbIngredients []dbmodels.IngredientWithUnit

	err := r.findIngredientsWithUnits().All(&dbIngredients)

	if err != nil {
		return nil, fmt.Errorf("failed to load ingredients from database: %w", err)
	}

	ingredients := []ingredient.Ingredient{}
	for _, dbIngredient := range dbIngredients {
		i, err := dbIngredient.ToDomain()

		if err != nil {
			return nil, fmt.Errorf("failed to map database ingredient to domain: %w", err)
		}

		ingredients = append(ingredients, i)
	}

	return ingredients, nil
}

// FindByIDs finds ingredients with provided ids in the database. Returns error when some ids are not found.
func (r ingredientDbRepository) FindByIDs(ids []uuid.UUID) ([]ingredient.Ingredient, error) {
	uniqueIDs := UniqueIDs(ids)
	dbIngredients := []dbmodels.IngredientWithUnit{}

	err := r.findIngredientsWithUnits().Where("i.id IN ?", uniqueIDs).All(&dbIngredients)
	if err != nil {
		return nil, fmt.Errorf("failed to load ingredients from database: %w", err)
	}

	if len(dbIngredients) != len(uniqueIDs) {
		return nil, fmt.Errorf("failed to load all ingredients from dbs (id IN (%v))", uniqueIDs)
	}

	ingredients := []ingredient.Ingredient{}
	for _, dbIngredient := range dbIngredients {
		i, err := dbIngredient.ToDomain()
		if err != nil {
			return nil, fmt.Errorf("failed to map database ingredient to domain: %w", err)
		}

		ingredients = append(ingredients, i)
	}

	return ingredients, nil
}

// Create creates new ingredient in the database
func (r ingredientDbRepository) Create(i ingredient.Ingredient) error {
	dbIngredient := dbmodels.NewIngredientFromDomain(i)
	_, err := r.ingredients().Insert(dbIngredient)

	if err != nil {
		return fmt.Errorf("failed to create ingredient: %w", err)
	}

	return nil
}

// Update updates existing ingredient in the database
func (r ingredientDbRepository) Update(i ingredient.Ingredient) error {
	dbIngredient := dbmodels.NewIngredientFromDomain(i)
	err := r.ingredients().Find(dbIngredient.ID).Update(dbIngredient)

	if err != nil {
		return fmt.Errorf("failed to update ingredient: %w", err)
	}

	return nil
}

// NewIngredientRepository creates database repository for ingredients
func NewIngredientRepository(session db.Session) ingredient.Repository {
	repository := &ingredientDbRepository{
		session: session,
		ingredients: func() db.Collection {
			return session.Collection("ingredients")
		},
	}

	return repository
}
