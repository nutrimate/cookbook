package repositories

import (
	"fmt"

	"gitlab.com/nutrimate/cookbook/internal/repositories/dbmodels"

	"github.com/google/uuid"
	"github.com/upper/db/v4"

	"gitlab.com/nutrimate/cookbook/pkg/mealtype"
)

type mealTypeDbRepository struct {
	mealTypes func() db.Collection
}

// FindAll returns all meal types found in the database
func (r mealTypeDbRepository) FindAll() ([]mealtype.MealType, error) {
	var dbMealTypes []dbmodels.MealType
	err := r.mealTypes().Find().All(&dbMealTypes)
	if err != nil {
		return nil, fmt.Errorf("failed to load meal types from database: %w", err)
	}

	mealTypes := []mealtype.MealType{}
	for _, dbMealType := range dbMealTypes {
		mt := dbMealType.ToDomain()
		mealTypes = append(mealTypes, mt)
	}

	return mealTypes, nil
}

// FindByIDs finds meal types with provided ids in the database.  Returns error when some ids are not found.
func (r mealTypeDbRepository) FindByIDs(ids []uuid.UUID) ([]mealtype.MealType, error) {
	uniqueIDs := UniqueIDs(ids)
	dbMealTypes := []dbmodels.MealType{}

	err := r.mealTypes().Find("id IN ?", uniqueIDs).All(&dbMealTypes)
	if err != nil {
		return nil, fmt.Errorf("failed to load meal types from database: %w", err)
	}

	if len(dbMealTypes) != len(uniqueIDs) {
		return nil, fmt.Errorf("failed to all load meal types from database (id IN (%v))", uniqueIDs)
	}

	mealTypes := []mealtype.MealType{}
	for _, dbMealType := range dbMealTypes {
		mt := dbMealType.ToDomain()
		mealTypes = append(mealTypes, mt)
	}

	return mealTypes, nil
}

// NewMealTypeRepository creates database repository for meal types
func NewMealTypeRepository(session db.Session) mealtype.Repository {
	repository := &mealTypeDbRepository{
		mealTypes: func() db.Collection {
			return session.Collection("meal_types")
		},
	}

	return repository
}
