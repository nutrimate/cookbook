package repositories

import (
	"github.com/upper/db/v4"

	"gitlab.com/nutrimate/cookbook/pkg/ingredient"
	"gitlab.com/nutrimate/cookbook/pkg/mealtype"
	"gitlab.com/nutrimate/cookbook/pkg/recipe"
	"gitlab.com/nutrimate/cookbook/pkg/unit"
)

// Container is a container module that provides repositories
type Container interface {
	GetDBSession() db.Session
	GetIngredientRepository() ingredient.Repository
	GetMealTypeRepository() mealtype.Repository
	GetRecipeRepository() recipe.Repository
	GetUnitRepository() unit.Repository
}

type container struct {
	dbSession db.Session
}

// GetDBSession returns existing db session
func (c container) GetDBSession() db.Session {
	return c.dbSession
}

// GetIngredientRepository returns new ingredient repository
func (c container) GetIngredientRepository() ingredient.Repository {
	return NewIngredientRepository(c.dbSession)
}

// GetMealTypeRepository returns new meal type repository
func (c container) GetMealTypeRepository() mealtype.Repository {
	return NewMealTypeRepository(c.dbSession)
}

// GetRecipeRepository returns existing recipe repository
func (c container) GetRecipeRepository() recipe.Repository {
	return NewRecipeRepository(c.dbSession)
}

//GetUnitRepository returns existing unit repository
func (c container) GetUnitRepository() unit.Repository {
	return NewUnitRepository(c.dbSession)
}

// NewContainer creates container with repositories
func NewContainer(dbSession db.Session) Container {
	c := container{
		dbSession: dbSession,
	}

	return c
}
