package config

import "github.com/kelseyhightower/envconfig"

// Config contains application configuration
type Config struct {
	Port               int `default:"80"`
	Debug              bool
	AllowedOrigins     []string `split_words:"true"`
	DbConnectionString string   `required:"true" split_words:"true"`
	Auth               struct {
		JWKSURI string `required:"true" envconfig:"AUTH_JWKS_URI"`
	}
}

// Load creates config based on environment variables
func Load() (Config, error) {
	var c Config
	err := envconfig.Process("", &c)

	return c, err
}
